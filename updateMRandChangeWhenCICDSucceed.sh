#!/bin/bash

# Set variables
MR_ID=$1
PROJECT_ID=$CI_PROJECT_ID
GITLAB_TOKEN=$SK_CI_JOB_TOKEN
SERVICE_NOW_INSTANCE=$SK_SERVICENOW_INSTANCE_FULL
USERNAME=$SK_SERVICENOW_USERNAME
PASSWORD=$SK_SERVICENOW_PASSWORD
CHANGE_REQUEST_ID=$2
lastJob_ID=$3

COMMENT_API_URL="${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/merge_requests/${MR_ID}/notes"
API_BASE_URL="${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/jobs"

# Fetch and check the status of all jobs
JOBS=$(curl --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "${API_BASE_URL}" | jq -r '.[].id')

# Flag to check if any job failed
all_jobs_succeeded=true

echo "Iterate over jobs and check their status..."
echo "Last Job ID: ${lastJob_ID}"

for JOB_ID in $JOBS; do
  API_URL="${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/jobs/${JOB_ID}"
  JOB_STATUS=$(curl --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "${API_URL}" | jq -r '.status')
  JOB_NAME=$(curl --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "${API_URL}" | jq -r '.name')

  echo "Job Name:${JOB_NAME}, Job ID: ${JOB_ID}, Status: ${JOB_STATUS}"

  # Check if any job failed
  if [ "$JOB_ID" != "$lastJob_ID" ]; then
  
  if [ "$JOB_STATUS" != "success" ]; then
    all_jobs_succeeded=false
    
  fi

  fi
done

# Add a comment to the merge request if all jobs succeeded
if [ "$all_jobs_succeeded" = true ]; then
  # Add a comment to the merge request
  COMMENT="✅ GitLab CI/CD Pipeline Passed Successfully!"
  curl --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --data "body=$COMMENT" "https://gitlab.com/api/v4/projects/$PROJECT_ID/merge_requests/$MR_ID/notes"


  # Update comments in the ServiceNow Change 
  COMMENT_MESSAGE="✅ GitLab CI/CD Pipeline Passed Successfully!"
  SERVICE_NOW_API="https://${SERVICE_NOW_INSTANCE}/api/now/table/change_request/$CHANGE_REQUEST_ID"

  curl --request PUT \
    --url "${SERVICE_NOW_API}" \
    --user "${USERNAME}:${PASSWORD}" \
    --header "Accept: application/json" \
    --header "Content-Type: application/json" \
    --data "{
      \"comments\": \"$COMMENT_MESSAGE\"
    }"
fi

