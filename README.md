Sandeep Kumar from ZION Team

Created Date : 1 December 2023

Version: 1.2.2 Draft
Modified Date: 16 Jan 2024

# Table of Contents

- [Table of Contents](#table-of-contents)
- [Business Requirement](#business-requirement)
    - [Assumptions, Dependencies, and Constraints:](#assumptions-dependencies-and-constraints)
    - [Functional Criteria:](#functional-criteria)
    - [Flow Diagram V1.0:](#flow-diagram-v10)
    - [Flow Diagram V2.0:](#flow-diagram-v20)
    - [Flow Diagram V3.0:](#flow-diagram-v30)
    - [Flow Diagram V4.0:](#flow-diagram-v40)
    - [Prerequisites](#prerequisites)
- [GitLab](#gitlab)
    - [GitLab instance or account creation](#gitlab-instance-or-account-creation)
    - [Projects in GitLab](#projects-in-gitlab)
    - [Create Merger Request](#create-merger-request)
    - [Variables Access Token](#variables-access-token)
    - [Create a branch](#create-a-branch)
    - [Commit and update](#commit-and-update)
    - [CI/CD Pipeline Behaviour GitLab](#cicd-pipeline-behaviour-gitlab)
- [YAML and Bash script Files](#yaml-and-bash-script-files)
  - [CI/CD pipeline YAML file configuration](#cicd-pipeline-yaml-file-configuration)
    - [YAML File configuration v 1.0](#yaml-file-configuration-v-10)
    - [YAML File configuration V 2.0](#yaml-file-configuration-v-20)
    - [YAML File configuration V 3.0](#yaml-file-configuration-v-30)
    - [YAML File configuration V 4.0](#yaml-file-configuration-v-40)
    - [YAML File configuration V 5.0](#yaml-file-configuration-v-50)
  - [Shell Script used in CI/CD pipeline](#shell-script-used-in-cicd-pipeline)
    - [Shell script to update merge request](#shell-script-to-update-merge-request)
    - [Shell script to update ServiceNow DevOps Change](#shell-script-to-update-servicenow-devops-change)
    - [Shell script to fetch merge request](#shell-script-to-fetch-merge-request)
    - [Shell script to fetch ServiceNow DevOps Change](#shell-script-to-fetch-servicenow-devops-change)
    - [Shell script to upload attachment in ServiceNow DevOps Change](#shell-script-to-upload-attachment-in-servicenow-devops-change)
    - [Shell script to update the comments in ServiceNow and Merge Request when Pipeline fails](#shell-script-to-update-the-comments-in-servicenow-and-merge-request-when-pipeline-fails)
    - [YAML script sample to upload pipeline logs as attachment in ServiceNow DevOps Change](#yaml-script-sample-to-upload-pipeline-logs-as-attachment-in-servicenow-devops-change)
- [ServiceNow DevOps Change Velocity](#servicenow-devops-change-velocity)
    - [Key Features](#key-features)
    - [Personas](#personas)
  - [Requirement](#requirement)
  - [Plugins](#plugins)
  - [Configuration](#configuration)
    - [Install Plugin](#install-plugin)
    - [Configure DevOps Change Velocity](#configure-devops-change-velocity)
    - [Onboard Gitlab](#onboard-gitlab)
    - [Onboard GitLab to DevOps Change Velocity --- Workspace](#onboard-gitlab-to-devops-change-velocity-----workspace)
    - [Connect to a Tool](#connect-to-a-tool)
    - [Create an Application](#create-an-application)
    - [Create DevOps Change](#create-devops-change)
    - [View pipeline in ServiceNow](#view-pipeline-in-servicenow)
- [Resource](#resource)


# Business Requirement 

1.  Utilization of the [ServiceNow DevOps (https://www.servicenow.com/products/devops.html) product to quickly jumpstart integration between ServiceNow and Gitlab

    a.  Partnership with ServiceNow to enhance and grow usage of their DevOps product while also expanding SaC capability.

2.  Development of a Gitlab CICD Pipeline to integrate with ServiceNow
    DevOps

    a.  Integration with ServiceNow should provide an approval gate before deploying to production

3.  Customer Documentation/Guides for proper setup

### Assumptions, Dependencies, and Constraints:

The developed solution of this feature should build upon the POC of
ServiceNow DevOps and Gitlab CI. The approach should be:

-   Add Gitlab to the ServiceNow DevOps Change Velocity list of tools

-   Modify the existing Gitlab CICD pipeline from terraform-aac so that
    a manual step can be mapped to change control in ServiceNow

-   Enhance the existing Gitlab CICD pipeline that runs on Merge to:

    -   Await approval from ServiceNow before deploying to production

    -   Add CHG details to associated Gitlab MR/Issue

    -   Enhance the ServiceNow CHG that is created through additional
        scripts in the pipeline that do things such as:

        -   Add notes on test success/failure

        -   Link to Gitlab Merge Request

        -   Add SaC specific implementation steps to CHG(describe that
            change is done through terraform etc).

        -   Bring Merge Request summary to CHG

-   Add notes/context to the CHG after the deploy occurs in Gitlab to
    dictate if the deployment succeeded or failed.

### Functional Criteria:

-   Develop ServiceNow DevOps and Gitlab integration and pipeline
    configuration that provides deployment gating (approval from
    ServiceNow) before pushing to production

-   Enhance the ServiceNow Change and Gitlab MR/Issue with relevant
    information

    -   Adding textual details of the Merge Request to ServiceNow Change
        Request Description

    -   Upload log.html to ServiceNow CHG if file is available in
        pipeline

    -   etc

-   Utilize GitLab environments if in community version -- set to
    environment production on deploy

-   Ensuring testing output from Gitlab pipeline (JUnit) is attached
    ServiceNow CHG.

    -   Being able to show the testing results in the CHG is very
        important to validate change success.

    -   If there are failures in the testing pipeline, add comment to
        ServiceNow work notes with summary of test failures

-   Add notes/context to the CHG after the deploy occurs in Gitlab to
    dictate if the deployment succeeded or failed.

-   Develop custom step functions in the SaC Gitlab Pipeline to enhance
    the ServiceNow integration

    -   Ensure any developed function can be customized for different
        environments

    -   Ensure functions can be reused for integrations with other ITSMs
        when possible. For example, a function to update the merge
        request with ITSM Change details could be reused for Gitlab,
        GitHub, JIRA, etc

    -   Functionality to develop:

        -   Update Merge Request with ServiceNow Change details

            -   In the Gitlab Pipeline, after the change is created, the
                merge request should be updated with the associated
                change request

                -   Change Number

                -   URL To ServiceNow Change

                -   Description

                -   State

                -   Scheduled Start Date

        -   Update Issue with ServiceNow Change details

            -   If Merge Request and its associated pipeline will close
                a issue, update the issue with the associated change
                information.

                -   Change Number

                -   URL To ServiceNow Change

                -   Description

                -   State

                -   Scheduled Start Date

        -   Update Merge Request with Terraform plan information

        -   Update ServiceNow CHG with SaC Information

            -   Upon CHG creation:

                -   Update CHG Short Description with \"SaC
                    Deployment:\" + Merge Request Title

                -   Update CHG Description with Merge Request
                    Description

                    -   [STRETCH\] Summarize changes

            -   Upon Implement/Deploy:

                -   Update CHG Notes with success/failure of deploy &
                    test stages

                -   Update CHG Notes with any failed tests

                -   Add log.html to CHG

                -   Ensure Tests are stored in DevOps Test table

        -   Validate CHG State

            -   Create step in pipeline before the deploy stage to check
                with ServiceNow that CHG is approved and between
                scheduled time window.

                -   If not in window, wait till timeout (default 1 hour,
                    retry 10 minutes). Output CHG status to console log
                    and MR notes

-   Update Merge Request with ServiceNow Change details upon change.

    -   On a routine basis (suggest Gitlab crone job) , for change
        requests not yet to the implement state, update the merge
        request if the following items change:

        -   State

        -   Approval Status

        -   Scheduled Start Date

### Flow Diagram V1.0:

![](./media/image1.png)

### Flow Diagram V2.0:

![](./media/image2.png)

### Flow Diagram V3.0:

![](./media/image3.png)

### Flow Diagram V4.0:

![](./media/image4.png)

### Prerequisites

1.  ServiceNow (Pro/Enterprise licensing )

2.  Gitlab 16.2+

3.  Network access from ServiceNow

# GitLab

### GitLab instance or account creation

Creating a GitLab account involves signing up on the GitLab platform.
Here are the general steps to create a GitLab account:

1.  Go to the GitLab Website:

    a.  Open your web browser and navigate to the GitLab website.

    b.  https://gitlab.com/users/sign_in

2.  Sign Up:

    a.  On the GitLab homepage, look for a \"Sign Up\" or \"Register\" button.

    b.  Click on it to begin the registration process.

3.  Provide Information:

    a.  Fill out the registration form with the required information.

    b.  You typically need to provide a valid email address, choose a
       username, and set a password.

![](./media/image5.png)

4.  Complete CAPTCHA or Verification:

    a.  Some platforms might have a CAPTCHA or additional verification
       steps to ensure you are a real person.

5.  Verify Email:

    a.  After submitting the registration form, you may need to verify your email address. GitLab will send a verification email to the
       address you provided.

6.  Verify Email Address:

    a.  Open the email from GitLab and click on the verification link or follow the instructions provided to verify your email address.

7.  Set Up Profile:

    a.  Once your email is verified, you might be prompted to set up your profile by adding additional information, such as a picture, bio, etc.

### Projects in GitLab

A project is a space where you manage your source code, collaborate with
team members, and track the progress of your work.

Projects in GitLab can be private or public, and they serve as
containers for your repositories, issues, merge requests, and more. Here
are some key aspects of projects in GitLab:

1.  Creating a Project:

    a.  In the upper right corner, click on the \"+\" icon, and then select \"New project.\"

    b.  Follow the prompts to configure your project, including its name, visibility (private or public), and other settings.

![A screenshot of a social media post Description automatically
generated](./media/image6.png)

2.  Project Visibility:

    a.  GitLab projects can be set as private, internal, or public.

       - Private projects are visible only to project members.
	   - Internal projects are visible to any authenticated user.
	   -  Public projects are visible to everyone, including those without a GitLab account.

3.  Project Features

    a.  GitLab projects come with various features, including:

       - Repository: Store your source code and manage branches.
       - Issues: Track and manage tasks, bugs, or feature requests.
       - Merge Requests: Propose and review changes to your codebase.
       - Wiki: Create and edit project documentation.
       - CI/CD Pipelines: Automate testing and deployment processes.
       - Issues Boards: Manage and visualize your work using boards.
       - Snippets: Share code snippets with others.
       - Settings: Configure project-specific settings.

4.  Repository

    a.  Each project has its own Git repository where you can store and version your source code.

    b.  You can clone the repository locally using Git commands or GitLab\'s web interface.

5.  Merge Requests

    a.  Propose changes to the codebase through merge requests.

    b.  Code changes can be reviewed, discussed, and merged into the main branch.

6.  CI/CD Pipelines:

    a.  GitLab provides built-in Continuous Integration and Continuous Deployment (CI/CD) features.

    b.  Automate testing and deployment processes using .gitlab-ci.yml configuration.

Collaboration:

### Create Merger Request

Merge Request Propose changes to the codebase through merge requests.
Which can be reviewed, discussed, and merged into the main branch.

![](./media/image7.png)

### Variables Access Token

Variables store information, like passwords and secret keys, that you
can use in job scripts. Each project can define a maximum of 8000
variables. [Learn more.](https://gitlab.com/help/ci/variables/index)

Variables can have several attributes. [Learn
more.](https://gitlab.com/help/ci/variables/index#define-a-cicd-variable-in-the-ui)

-   Protected: Only exposed to protected branches or protected tags.

-   Masked: Hidden in job logs. Must match masking requirements.

-   Expanded: Variables with \$ will be treated as the start of a
    reference to another variable.

![](./media/image8.png)

### Create a branch

A branch is a parallel version of a repository\'s codebase that can be
modified independently of the main branch (typically the 'main' or
'master' branch). Branches are useful for developing features, bug
fixes, or experiments without affecting the main codebase until changes
are ready to be merged.

**Default Branch:**

When you create a new project, GitLab usually sets the default branch to
master.

The default branch is the primary branch where the latest stable code is
maintained.

**Creating a Branch**

Go to your project and click on + icon

![](./media/image9.png)

### Commit and update

Committing and updating code involves a series of steps that you
typically perform using Git commands

### CI/CD Pipeline Behaviour GitLab

The behavior you\'re describing is typical in GitLab when it comes to
pipelines and merge requests. GitLab triggers pipelines in different
stages of the merge request lifecycle. Here\'s a breakdown of why you
might see two pipeline runs:

1.  Pipeline Before Merge (Pre-Merge):

    a.  Trigger: When you create a merge request, GitLab automatically triggers a pipeline to run the CI/CD jobs associated with the branch you are merging into. This allows you to validate your changes before they are merged.

    b.  Purpose: The purpose of this pipeline is to check the quality and correctness of your changes before they are integrated into  the target branch.

2.  Pipeline After Merge (Post-Merge):

    a.  Trigger: After the merge request is accepted and merged into the target branch (e.g., main), GitLab triggers another pipeline for the target branch.

    b.  Purpose: This pipeline is triggered on the target branch (post-merge) and is typically used to deploy or perform additional tasks that should happen after the changes are integrated.

In summary, the pre-merge pipeline is triggered when the merge request
is created, and the post-merge pipeline is triggered when the merge
request is merged into the target branch. Both serve different purposes
in the CI/CD process.

# YAML and Bash script Files

## CI/CD pipeline YAML file configuration

### YAML File configuration v 1.0
```yaml
variables:
  PipelineID: $CI_PIPELINE_ID
    # Description: ID of Pipeline running currently 
  SK_SNOW_CHANGE_API: https://${SK_SERVICENOW_INSTANCE_FULL}/api/now/table/change_request?sysparm_query=implementation_planLIKE${PipelineID}&sysparm_display_value=true&sysparm_limit=1 

stages:          
  - build
  - fetch_mergeRequest_details
  - fetch_snowDevOpsChange_details
  - update_snowDevOpsChange
  - update_merge_request
  - wait_for_ChangeApproval_stateImplemented
  - deploy
  
  
build-job:       # This job runs in the build stage, which runs first.
  stage: build
  script:
    - echo "Compiling the code..."
    - echo "Compile complete."

fetch_mergeRequest_details:
  stage: fetch_mergeRequest_details
  script:
    - apt-get update -qy
    - apt-get install -y curl jq
    
    - |
      MR_IID=$(curl -s --request GET \
        --header "Content-Type: application/json" \
        --header "Authorization: Bearer $SK_CI_JOB_TOKEN" \
        "${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/merge_requests/" | jq -r '.[0].iid')
    
      MR_DETAILS=$(curl -s --request GET \
        --header "Content-Type: application/json" \
        --header "Authorization: Bearer $SK_CI_JOB_TOKEN" \
        "${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/merge_requests/${MR_IID}")
      
    # Create artifacts directory
    - mkdir -p artifacts
    
    #- echo "MRDetails:" $MR_DETAILS
    - MR_Title=$(echo $MR_DETAILS | jq -r '.title')
    - MR_ID=$(echo $MR_DETAILS | jq -r '.id')
    - MR_IID=$(echo $MR_DETAILS | jq -r '.iid')
    - MR_State=$(echo $MR_DETAILS | jq -r '.state')
    
    - echo "Merge Request Details:-" "Title:" $MR_Title  " Id:" $MR_ID " IID:" $MR_IID " State:" $MR_State
    
    # Exporting variables to use in other jobs
    - echo "export MR_Title=\"$MR_Title\"" >> artifacts/envariables.sh
    - echo "export MR_ID=\"$MR_ID\"" >> artifacts/envariables.sh
    - echo "export MR_IID=\"$MR_IID\"" >> artifacts/envariables.sh
    - echo "export MR_State=\"$MR_State\"" >> artifacts/envariables.sh
  artifacts:
    paths:
      - artifacts/
  only:
    - main
fetch_snowDevOpsChange_details:
  stage: fetch_snowDevOpsChange_details
  script:
    - apt-get update -qy
    - apt-get install -y curl jq
    - source artifacts/envariables.sh

    - echo "MR_Title:" $MR_Title
    - echo "MR_ID:" $MR_ID
    - echo "MR_IID:" $MR_IID
    - echo "MR_State:" $MR_State

    # Make the REST API request using curl
    #- echo "SK_SNOW_CHANGE_API:" ${SK_SNOW_CHANGE_API}
    - responseForChgSysID=$(curl -s -u "${SK_SERVICENOW_USERNAME}:${SK_SERVICENOW_PASSWORD}" "${SK_SNOW_CHANGE_API}")
    #- echo "responseForChgSysID:" ${responseForChgSysID}
    - ChgSoDec=$(echo $responseForChgSysID | jq -r '.result[0].short_description')
    - ChgDec=$(echo $responseForChgSysID | jq -r '.result[0].description')
    - ChgSysID=$(echo $responseForChgSysID | jq -r '.result[0].sys_id')
    - ChgState=$(echo $responseForChgSysID | jq -r '.result[0].state')
    - ChgNumber=$(echo $responseForChgSysID | jq -r '.result[0].number')

    - echo "ServiceNow Change Details:" " ChgNumber:" $ChgNumber ", ChgState:" $ChgState ",ChgSoDec:" $ChgSoDec ", ChgSysID:" $ChgSysID 
   
   # Exporting variables to use in other jobs
    - echo "export MR_Title=\"$MR_Title\"" >> artifacts/envariables.sh
    - echo "export MR_ID=\"$MR_ID\"" >> artifacts/envariables.sh
    - echo "export MR_IID=\"$MR_IID\"" >> artifacts/envariables.sh
    - echo "export MR_State=\"$MR_State\"" >> artifacts/envariables.sh
    

   # Exporting variables to use in other jobs
    - echo "export ChgSoDec=\"$ChgSoDec\"" >> artifacts/envariables.sh
    - echo "export ChgDec=\"$ChgDec\"" >> artifacts/envariables.sh
    - echo "export ChgSysID=\"$ChgSysID\"" >> artifacts/envariables.sh
    - echo "export ChgState=\"$ChgState\"" >> artifacts/envariables.sh
    - echo "export ChgNumber=\"$ChgNumber\"" >> artifacts/envariables.sh
    
  artifacts:
    paths:
      - artifacts/
  only:
    - main
   
update_snowDevOpsChange:
  stage: update_snowDevOpsChange
  script:
    - source artifacts/envariables.sh
    # Import Change Details to combine with Merger Request details
    - echo "ChgSoDec:" $ChgSoDec
    - echo "ChgDec:" $ChgDec
    - echo "ChgSysID:" $ChgSysID
    - echo "ChgState:" $ChgState
    - echo "ChgNumber:" $ChgNumber

    # Import Merger Request details
    - echo "MR_Title:" $MR_Title
    - echo "MR_ID:" $MR_ID
    - echo "MR_IID:" $MR_IID
    - echo "MR_State:" $MR_State
    
    #Form a gitLab Merge Request URL
    - GITLAB_ISSUE_LINK=$(echo "[code]<a href=\"https://gitlab.com/sandeepiglasia1/myfirstprojectcopy/merge_requests/$MR_IID\"target="_blank">Link to GitLab Merge Request</a>[/code]")
    
    - SK_CHANGE_API_USINGSYSID="https://${SK_SERVICENOW_INSTANCE_FULL}/api/now/table/change_request/$ChgSysID"
    - echo "SK_CHANGE_API_USINGSYSID:" $SK_CHANGE_API_USINGSYSID

    - updatedShortDescription=$(echo $ChgSoDec ", MR_Title:" $MR_Title ", MR_ID:" $MR_ID ", MR_IID:" $MR_IID)
    - updatedDescription=$(echo $ChgDec ", MR_Title:" $MR_Title ", MR_ID:" $MR_ID ", MR_IID:" $MR_IID)
    - updatedWorkNotes=$(echo "Associated Merge Request Details:" $GITLAB_ISSUE_LINK ", MR_Title:" $MR_Title ", MR_ID:" $MR_ID ", MR_IID:" $MR_IID)
    #- updatedWorkNotes=$(echo "Associated Merge Request:" $GITLAB_ISSUE_LINK )
    - echo "updatedDescription:" $updatedDescription
    - echo "updatedShortDescription:" $updatedShortDescription

    - >
      curl -X PATCH
      -u $SK_SERVICENOW_USERNAME:$SK_SERVICENOW_PASSWORD
      $SK_CHANGE_API_USINGSYSID
      -H "Content-Type: application/json"
      -H "Accept: application/json"
      -d '{"work_notes": "'"${updatedWorkNotes//\"/\\\"}"'","short_description": "'"$updatedShortDescription"'", "description": "'"$updatedDescription"'"}'
  only:
    - main  # Adjust as needed, depending on when you want to trigger this job

update_merge_request:
  stage: update_merge_request
  script:
    - source artifacts/envariables.sh
    #Import Change Details to combine with Merger Request details
    - echo "ChgSoDec:" $ChgSoDec
    - echo "ChgDec:" $ChgDec
    - echo "ChgSysID:" $ChgSysID
    - echo "ChgState:" $ChgState
    - echo "ChgNumber:" $ChgNumber

    #Import Merger Request details
    - echo "MR_Title:" $MR_Title
    - echo "MR_ID:" $MR_ID
    - echo "MR_IID:" $MR_IID
    - echo "MR_State:" $MR_State
  
    - contentToUpdate=$(echo "ServiceNow Change Details:" "[${ChgNumber}](https://${SK_SERVICENOW_INSTANCE_FULL}/change_request.do?sys_id=${ChgSysID})" ",State :" $ChgState  ",Short Description:" $ChgSoDec )

    - |
      curl --request POST \
        --header "Content-Type: application/json" \
        --header "Authorization: Bearer $SK_CI_JOB_TOKEN" \
        --data "{\"body\":\"$contentToUpdate\"}" \
        "${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/merge_requests/${MR_IID}/notes"
  only:
    - main

  
wait_for_ChangeApproval_stateImplemented:      
  stage: wait_for_ChangeApproval_stateImplemented  

  before_script:
    - chmod +x ./fetch_snowDevOpsChange_details.sh

  script:
    
    - apt-get update -qy
    - apt-get install -y curl jq

    # Fetch the change state until its in implement state
    # Set the timeout (in seconds)
    
    - export TIMEOUT=60  # 1 minutes timeout
    - export CHANGE_STATE=""
    - export START_TIME=$(date +%s)
    - |
      while [ "$CHANGE_STATE" != "Implement" ]; do
      CURRENT_TIME=$(date +%s)
      ELAPSED_TIME=$((CURRENT_TIME - START_TIME))

      if [ "$ELAPSED_TIME" -ge "$TIMEOUT" ]; then
        echo "Timeout reached. Exiting the loop."
        break
      fi

      # Fetch the change Details
      changeDetails=$(./fetch_snowDevOpsChange_details.sh $CI_PIPELINE_ID)
      CHANGE_STATE=$(echo "$changeDetails" | jq -r '.result[0].state')
      ChangeApproval=$(echo "$changeDetails" | jq -r '.result[0].approval')
      echo $CHANGE_STATE
      echo $ChangeApproval
      sleep 10s
      done

      if [ "$CHANGE_STATE" = "Implement" ]; then
      echo "Change is now in Implement state. Proceeding with deployment."
      else
      echo "Timeout reached. Deployment aborted."
      exit 1
      fi
  only:
    - main
     
deploy:
    stage: deploy
    #when: manual
    allow_failure: false
    script:
      - echo 'prod'
  

```
### YAML File configuration V 2.0

```yaml
variables:
  GIT_SSL_NO_VERIFY: "true"

  SK_CI_JOB_TOKEN:
    description: "GitLab Access Token. Used to create or update comments on Merge Requests"
  SK_GITLAB_URL:
    description: "GitLab url used to create a API "
  SK_SERVICENOW_INSTANCE_FULL:
    description: "ServiceNow instance URL to form an API to connect with ServiceNow"
  SK_SERVICENOW_PASSWORD:
    description: "ServiceNow instance password to provide authentication"
  SK_SERVICENOW_USERNAME:
    description: "ServiceNow User Name for API authentication"
  

  PipelineID: $CI_PIPELINE_ID
  SK_SNOW_CHANGE_API: https://${SK_SERVICENOW_INSTANCE_FULL}/api/now/table/change_request?sysparm_query=implementation_planLIKE${PipelineID}&sysparm_display_value=true&sysparm_limit=1 
 

# List of stages for jobs, and their order of execution
stages:          
  - build
  - fetch_mergeRequest_details
  - fetch_snowDevOpsChange_details
  - update_snowDevOpsChange
  - update_merge_request
  - wait_for_ChangeApproval_stateImplemented
  - deploy
  - wait_forAll_jobCompletion
  - upload_logTo_serviceNow
  
  
build-job:       # This job runs in the build stage, which runs first.
  stage: build
  script:
    - echo "************************ build Job Running ********************************************************************"
    - echo "Compiling the code..."
    - echo "Compile complete."

fetch_mergeRequest_details:
  stage: fetch_mergeRequest_details
  script:
    - echo "************************ fetch_mergeRequest_details Job Running ********************************************************************"
    - apt-get update -qy
    - apt-get install -y curl jq
    - echo "SK_GITLAB_URL:" $SK_GITLAB_URL
    
    
    
    - |
      MR_IID=$(curl -s --request GET \
        --header "Content-Type: application/json" \
        --header "Authorization: Bearer $SK_CI_JOB_TOKEN" \
        "${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/merge_requests/" | jq -r '.[0].iid')
    
      MR_DETAILS=$(curl -s --request GET \
        --header "Content-Type: application/json" \
        --header "Authorization: Bearer $SK_CI_JOB_TOKEN" \
        "${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/merge_requests/${MR_IID}")
      
    # Create artifacts directory
    - mkdir -p artifacts
    
    - echo "MRDetails "
    - MR_Title=$(echo $MR_DETAILS | jq -r '.title')
    - MR_ID=$(echo $MR_DETAILS | jq -r '.id')
    - MR_IID=$(echo $MR_DETAILS | jq -r '.iid')
    - MR_State=$(echo $MR_DETAILS | jq -r '.state')
    
    - echo "Merge Request Details:-" "Title:" $MR_Title  " Id:" $MR_ID " IID:" $MR_IID " State:" $MR_State
    
    - echo "Exporting variables to use in other jobs"
    
    - echo "export MR_Title=\"$MR_Title\"" >> artifacts/envariables.sh
    - echo "export MR_ID=\"$MR_ID\"" >> artifacts/envariables.sh
    - echo "export MR_IID=\"$MR_IID\"" >> artifacts/envariables.sh
    - echo "export MR_State=\"$MR_State\"" >> artifacts/envariables.sh
  artifacts:
    paths:
      - artifacts/
  only:
    - main
fetch_snowDevOpsChange_details:
  stage: fetch_snowDevOpsChange_details
  script:
    - echo "************************ fetch_snowDevOpsChange_details Job Running ********************************************************************"
    - apt-get update -qy
    - apt-get install -y curl jq
    - source artifacts/envariables.sh
    
    - echo "Merger Request Details"
    
    - echo "MR_Title:" $MR_Title
    - echo "MR_ID:" $MR_ID
    - echo "MR_IID:" $MR_IID
    - echo "MR_State:" $MR_State

     responseForChgSysID=$(curl -s -u "${SK_SERVICENOW_USERNAME}:${SK_SERVICENOW_PASSWORD}" "${SK_SNOW_CHANGE_API}")
    - ChgSoDec=$(echo $responseForChgSysID | jq -r '.result[0].short_description')
    - ChgDec=$(echo $responseForChgSysID | jq -r '.result[0].description')
    - ChgSysID=$(echo $responseForChgSysID | jq -r '.result[0].sys_id')
    - ChgState=$(echo $responseForChgSysID | jq -r '.result[0].state')
    - ChgNumber=$(echo $responseForChgSysID | jq -r '.result[0].number')

    - echo "ServiceNow Change Details:" " ChgNumber:" $ChgNumber ", ChgState:" $ChgState ",ChgSoDec:" $ChgSoDec ", ChgSysID:" $ChgSysID 
   
    - echo "Exporting variables to use in other jobs"

    - echo "export MR_Title=\"$MR_Title\"" >> artifacts/envariables.sh
    - echo "export MR_ID=\"$MR_ID\"" >> artifacts/envariables.sh
    - echo "export MR_IID=\"$MR_IID\"" >> artifacts/envariables.sh
    - echo "export MR_State=\"$MR_State\"" >> artifacts/envariables.sh
    

    - echo "Exporting variables to use in other jobs"

    - echo "export ChgSoDec=\"$ChgSoDec\"" >> artifacts/envariables.sh
    - echo "export ChgDec=\"$ChgDec\"" >> artifacts/envariables.sh
    - echo "export ChgSysID=\"$ChgSysID\"" >> artifacts/envariables.sh
    - echo "export ChgState=\"$ChgState\"" >> artifacts/envariables.sh
    - echo "export ChgNumber=\"$ChgNumber\"" >> artifacts/envariables.sh
    
  artifacts:
    paths:
      - artifacts/
  only:
    - main
   
update_snowDevOpsChange:
  stage: update_snowDevOpsChange
  script:
    - echo "************************ update_snowDevOpsChange Job Running ********************************************************************"
    - source artifacts/envariables.sh
    - echo "Import Change Details to combine with Merger Request details....."
    - echo "ChgSoDec:" $ChgSoDec
    - echo "ChgDec:" $ChgDec
    - echo "ChgSysID:" $ChgSysID
    - echo "ChgState:" $ChgState
    - echo "ChgNumber:" $ChgNumber

    - echo "Import Merger Request details......"
    - echo "MR_Title:" $MR_Title
    - echo "MR_ID:" $MR_ID
    - echo "MR_IID:" $MR_IID
    - echo "MR_State:" $MR_State
    
    #Form a gitLab Merge Request URL
    - PROJECT_URL="https://gitlab.com/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME"
    - echo "Project URL:"" $PROJECT_URL"
    - GITLAB_ISSUE_LINK=$(echo "[code]<a href=\"$PROJECT_URL/merge_requests/$MR_IID\"target="_blank">Link to GitLab Merge Request</a>[/code]")
    
    - SK_CHANGE_API_USINGSYSID="https://${SK_SERVICENOW_INSTANCE_FULL}/api/now/table/change_request/$ChgSysID"
    - echo "SK_CHANGE_API_USINGSYSID:" $SK_CHANGE_API_USINGSYSID

    - updatedShortDescription=$(echo $ChgSoDec ", MR_Title:" $MR_Title ", MR_ID:" $MR_ID ", MR_IID:" $MR_IID)
    - updatedDescription=$(echo $ChgDec ", MR_Title:" $MR_Title ", MR_ID:" $MR_ID ", MR_IID:" $MR_IID)
    - updatedWorkNotes=$(echo "Associated Merge Request Details:" $GITLAB_ISSUE_LINK ", MR_Title:" $MR_Title ", MR_ID:" $MR_ID ", MR_IID:" $MR_IID)
    #- updatedWorkNotes=$(echo "Associated Merge Request:" $GITLAB_ISSUE_LINK )
    - echo "updatedDescription:" $updatedDescription
    - echo "updatedShortDescription:" $updatedShortDescription

    - >
      curl -X PATCH
      -u $SK_SERVICENOW_USERNAME:$SK_SERVICENOW_PASSWORD
      $SK_CHANGE_API_USINGSYSID
      -H "Content-Type: application/json"
      -H "Accept: application/json"
      -d '{"work_notes": "'"${updatedWorkNotes//\"/\\\"}"'","short_description": "'"$updatedShortDescription"'", "description": "'"$updatedDescription"'"}'
  only:
    - main  

update_merge_request:
  stage: update_merge_request
  script:
    
    - echo "************************ update_merge_request Job Running ********************************************************************"
   
    - source artifacts/envariables.sh

    - echo "Import Change Details to combine with Merger Request details....."

    - echo "ChgSoDec:" $ChgSoDec
    - echo "ChgDec:" $ChgDec
    - echo "ChgSysID:" $ChgSysID
    - echo "ChgState:" $ChgState
    - echo "ChgNumber:" $ChgNumber

    - echo "Import Merger Request details......"

    - echo "MR_Title:" $MR_Title
    - echo "MR_ID:" $MR_ID
    - echo "MR_IID:" $MR_IID
    - echo "MR_State:" $MR_State
  
    - contentToUpdate=$(echo "ServiceNow Change Details:" "[${ChgNumber}](https://${SK_SERVICENOW_INSTANCE_FULL}/change_request.do?sys_id=${ChgSysID})" ",State :" $ChgState  ",Short Description:" $ChgSoDec )

    - |
      curl --request POST \
        --header "Content-Type: application/json" \
        --header "Authorization: Bearer $SK_CI_JOB_TOKEN" \
        --data "{\"body\":\"$contentToUpdate\"}" \
        "${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/merge_requests/${MR_IID}/notes"
  only:
    - main

  
wait_for_ChangeApproval_stateImplemented:      
  stage: wait_for_ChangeApproval_stateImplemented  

  before_script:
    - chmod +x ./fetch_snowDevOpsChange_details.sh

  script:
    - echo "************************ wait_for_ChangeApproval_stateImplemented Job Running ********************************************************************"
    - apt-get update -qy
    - apt-get install -y curl jq

    # Fetch the change state until its in implement state
    # Set the timeout (in seconds)
    
    - export TIMEOUT=60  # 1 minutes timeout
    - export CHANGE_STATE=""
    - export START_TIME=$(date +%s)
    - |
      while [ "$CHANGE_STATE" != "Implement" ]; do
      CURRENT_TIME=$(date +%s)
      ELAPSED_TIME=$((CURRENT_TIME - START_TIME))

      if [ "$ELAPSED_TIME" -ge "$TIMEOUT" ]; then
        echo "Timeout reached. Exiting the loop."
        break
      fi

      # Fetch the change Details
      changeDetails=$(./fetch_snowDevOpsChange_details.sh $CI_PIPELINE_ID)
      CHANGE_STATE=$(echo "$changeDetails" | jq -r '.result[0].state')
      ChangeApproval=$(echo "$changeDetails" | jq -r '.result[0].approval')
      echo $CHANGE_STATE
      echo $ChangeApproval
      sleep 10s
      done

      if [ "$CHANGE_STATE" = "Implement" ]; then
      echo "Change is now in Implement state. Proceeding with deployment."
      else
      echo "Timeout reached. Deployment aborted."
      exit 1
      fi
  only:
    - main
     

     
deploy:
    stage: deploy
    #when: manual
    allow_failure: false
    script:

      - echo "************************ deploy Job Running ********************************************************************"
      - echo 'prod'
  

wait_forAll_jobCompletion:
  stage: wait_forAll_jobCompletion
  script:
    - echo "************************ wait_forAll_jobCompletion Job Running ********************************************************************"
    - echo "Sleeping for 80 seconds after all jobs in the pipeline"
    - sleep 80
  when: always

upload_logTo_serviceNow:
  stage: upload_logTo_serviceNow
  before_script:
    - chmod +x ./upload_attachment.sh
  script:
    - echo "************************ upload_logTo_serviceNow Job Running ********************************************************************"
    - source artifacts/envariables.sh
    - echo "Import Change Details to combine with Merger Request details....."
    - echo "ChgSoDec:" $ChgSoDec
    - echo "ChgDec:" $ChgDec
    - echo "ChgSysID:" $ChgSysID
    - echo "ChgState:" $ChgState
    - echo "ChgNumber:" $ChgNumber

    - echo "Import Merger Request details......"
    - echo "MR_Title:" $MR_Title
    - echo "MR_ID:" $MR_ID
    - echo "MR_IID:" $MR_IID
    - echo "MR_State:" $MR_State
    - mkdir -p trace_files
    - apt-get update -qy
    - apt-get install -y curl jq
    - API_BASE_URL="${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/jobs"
    - JOBS=$(curl --header "PRIVATE-TOKEN:"" $SK_CI_JOB_TOKEN" "$API_BASE_URL" | jq -r '.[].id')
    - echo "Iterate over jobs and download trace files....."
    - for JOB_ID in $JOBS; do
        API_URL="${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/jobs/$JOB_ID/trace";
        echo "Downloading trace file for Job ID:"" $JOB_ID";
        curl --location --header "PRIVATE-TOKEN:"" $SK_CI_JOB_TOKEN" "$API_URL" >> "trace_files/GitLabPipeline.${CI_PIPELINE_ID}.job.log";
      done
    - ls -la trace_files
    - ./upload_attachment.sh "${CI_PROJECT_DIR}/trace_files/GitLabPipeline.${CI_PIPELINE_ID}.job.log" "$ChgSysID"


```

### YAML File configuration V 3.0

```yaml

variables:
  GIT_SSL_NO_VERIFY: "true"

  SK_CI_JOB_TOKEN:
    description: "GitLab Access Token. Used to create or update comments on Merge Requests"
  SK_GITLAB_URL:
    description: "GitLab url used to create a API "
  SK_SERVICENOW_INSTANCE_FULL:
    description: "ServiceNow instance URL to form an API to connect with ServiceNow"
  SK_SERVICENOW_PASSWORD:
    description: "ServiceNow instance password to provide authentication"
  SK_SERVICENOW_USERNAME:
    description: "ServiceNow User Name for API authentication"
  
 

# List of stages for jobs, and their order of execution
stages:          
  - build
  - fetch_mergeRequest_details
  - fetch_snowDevOpsChange_details
  - update_snowDevOpsChange
  - update_merge_request
  - wait_for_ChangeApproval_stateImplemented
  - deploy
  - wait_forAll_jobCompletion
  - upload_logTo_serviceNow
  
  
build-job:       # This job runs in the build stage, which runs first.
  stage: build
  script:
    - echo "************************ build Job Running ********************************************************************"
    - echo "Compiling the code..."
    - echo "Compile complete."

fetch_mergeRequest_details:
  stage: fetch_mergeRequest_details
  script:
    - echo "************************ fetch_mergeRequest_details Job Running ********************************************************************"
    - apt-get update -qy
    - apt-get install -y curl jq
    - echo "SK_GITLAB_URL:" $SK_GITLAB_URL
    
    
    
    - |
      MR_IID=$(curl -s --request GET \
        --header "Content-Type: application/json" \
        --header "Authorization: Bearer $SK_CI_JOB_TOKEN" \
        "${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/merge_requests/" | jq -r '.[0].iid')
    
      MR_DETAILS=$(curl -s --request GET \
        --header "Content-Type: application/json" \
        --header "Authorization: Bearer $SK_CI_JOB_TOKEN" \
        "${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/merge_requests/${MR_IID}")
      
    # Create artifacts directory
    - mkdir -p artifacts
    
    - echo "MRDetails "
    - MR_Title=$(echo $MR_DETAILS | jq -r '.title')
    - MR_ID=$(echo $MR_DETAILS | jq -r '.id')
    - MR_IID=$(echo $MR_DETAILS | jq -r '.iid')
    - MR_State=$(echo $MR_DETAILS | jq -r '.state')
    
    - echo "Merge Request Details:-" "Title:" $MR_Title  " Id:" $MR_ID " IID:" $MR_IID " State:" $MR_State
    
    - echo "Exporting variables to use in other jobs"
    
    - echo "export MR_Title=\"$MR_Title\"" >> artifacts/envariables.sh
    - echo "export MR_ID=\"$MR_ID\"" >> artifacts/envariables.sh
    - echo "export MR_IID=\"$MR_IID\"" >> artifacts/envariables.sh
    - echo "export MR_State=\"$MR_State\"" >> artifacts/envariables.sh
  artifacts:
    paths:
      - artifacts/
  only:
    - main
fetch_snowDevOpsChange_details:
  stage: fetch_snowDevOpsChange_details
  script:
    - echo "************************ fetch_snowDevOpsChange_details Job Running ********************************************************************"
    - apt-get update -qy
    - apt-get install -y curl jq
    - source artifacts/envariables.sh
    
    - echo "Merger Request Details"
    
    - echo "MR_Title:" $MR_Title
    - echo "MR_ID:" $MR_ID
    - echo "MR_IID:" $MR_IID
    - echo "MR_State:" $MR_State

  # API and code to fetch pipeline execution sysid rom ServiceNow
    - SK_API_PIPELINE_EXECUTION="https://${SK_SERVICENOW_INSTANCE_FULL}/api/now/table/sn_devops_pipeline_execution?sysparm_query=build_number=$CI_PIPELINE_ID&sysparm_fields=sys_id&sysparm_limit=1" 

    - echo "API for ServiceNow Pipeline Execution table:" $SK_API_PIPELINE_EXECUTION

        pipelineRresponse=$(curl -s -u "${SK_SERVICENOW_USERNAME}:${SK_SERVICENOW_PASSWORD}" "${SK_API_PIPELINE_EXECUTION}")

    - pipelineExecutionSysid=$(echo "$pipelineRresponse" | jq -r '.result[0].sys_id')
    - echo "pipelineExecutionSysid:" $pipelineExecutionSysid 
   
  
  # API and code to fetch Step execution sysid from ServiceNow
    - SK_API_STEP_EXECUTION="https://${SK_SERVICENOW_INSTANCE_FULL}/api/now/table/sn_devops_step_execution?sysparm_query=pipeline_execution.sys_id=${pipelineExecutionSysid}^change_requestISNOTEMPTY&sysparm_fields=change_request&sysparm_limit=1" 

    - echo "API for ServiceNow Step Execution table:" $SK_API_STEP_EXECUTION

       responseWithChgSysID=$(curl -s -u "${SK_SERVICENOW_USERNAME}:${SK_SERVICENOW_PASSWORD}" "${SK_API_STEP_EXECUTION}")

    - chngSysID=$(echo "$responseWithChgSysID" | jq -r '.result[0].change_request.value')

    - echo "chngSysID:" $chngSysID 

  # API and code to ServiceNow Change Details
    - SK_API_SNOW_CHANGE_DETAILS="https://dev183160.service-now.com/api/now/table/change_request?sysparm_query=sys_idSTARTSWITH${chngSysID}&sysparm_display_value=true"

    - echo "API for ServiceNow Change REquest table:" $SK_API_SNOW_CHANGE_DETAILS

       responseWithChgDetails=$(curl -s -u "${SK_SERVICENOW_USERNAME}:${SK_SERVICENOW_PASSWORD}" "${SK_API_SNOW_CHANGE_DETAILS}")

    - echo "responseWithChgDetails:" $responseWithChgDetails

  # Parse the ServiceNow Change details
    - ChgSoDec=$(echo $responseWithChgDetails | jq -r '.result[0].short_description')
    - ChgDec=$(echo $responseWithChgDetails | jq -r '.result[0].description')
    - ChgSysID=$(echo $responseWithChgDetails | jq -r '.result[0].sys_id')
    - ChgState=$(echo $responseWithChgDetails | jq -r '.result[0].state')
    - ChgNumber=$(echo $responseWithChgDetails | jq -r '.result[0].number')

    - echo "ServiceNow Change Details:" " ChgNumber:" $ChgNumber ", ChgState:" $ChgState ",ChgSoDec:" $ChgSoDec ", ChgSysID:" $ChgSysID 
   
    - echo "Exporting variables to use in other jobs"

    - echo "export MR_Title=\"$MR_Title\"" >> artifacts/envariables.sh
    - echo "export MR_ID=\"$MR_ID\"" >> artifacts/envariables.sh
    - echo "export MR_IID=\"$MR_IID\"" >> artifacts/envariables.sh
    - echo "export MR_State=\"$MR_State\"" >> artifacts/envariables.sh
    

    - echo "Exporting variables to use in other jobs"

    - echo "export ChgSoDec=\"$ChgSoDec\"" >> artifacts/envariables.sh
    - echo "export ChgDec=\"$ChgDec\"" >> artifacts/envariables.sh
    - echo "export ChgSysID=\"$ChgSysID\"" >> artifacts/envariables.sh
    - echo "export ChgState=\"$ChgState\"" >> artifacts/envariables.sh
    - echo "export ChgNumber=\"$ChgNumber\"" >> artifacts/envariables.sh
    
  artifacts:
    paths:
      - artifacts/
  only:
    - main
   
update_snowDevOpsChange:
  stage: update_snowDevOpsChange
  script:
    - echo "************************ update_snowDevOpsChange Job Running ********************************************************************"
    - source artifacts/envariables.sh
    - echo "Import Change Details to combine with Merger Request details....."
    - echo "ChgSoDec:" $ChgSoDec
    - echo "ChgDec:" $ChgDec
    - echo "ChgSysID:" $ChgSysID
    - echo "ChgState:" $ChgState
    - echo "ChgNumber:" $ChgNumber

    - echo "Import Merger Request details......"
    - echo "MR_Title:" $MR_Title
    - echo "MR_ID:" $MR_ID
    - echo "MR_IID:" $MR_IID
    - echo "MR_State:" $MR_State
    
    #Form a gitLab Merge Request URL
    - PROJECT_URL="https://gitlab.com/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME"
    - echo "Project URL:"" $PROJECT_URL"
    - GITLAB_ISSUE_LINK=$(echo "[code]<a href=\"$PROJECT_URL/merge_requests/$MR_IID\"target="_blank">Link to GitLab Merge Request</a>[/code]")
    
    - SK_CHANGE_API_USINGSYSID="https://${SK_SERVICENOW_INSTANCE_FULL}/api/now/table/change_request/$ChgSysID"
    - echo "SK_CHANGE_API_USINGSYSID:" $SK_CHANGE_API_USINGSYSID

    - updatedShortDescription=$(echo $ChgSoDec ", MR_Title:" $MR_Title ", MR_ID:" $MR_ID ", MR_IID:" $MR_IID)
    - updatedDescription=$(echo $ChgDec ", MR_Title:" $MR_Title ", MR_ID:" $MR_ID ", MR_IID:" $MR_IID)
    - updatedWorkNotes=$(echo "Associated Merge Request Details:" $GITLAB_ISSUE_LINK ", MR_Title:" $MR_Title ", MR_ID:" $MR_ID ", MR_IID:" $MR_IID)
    #- updatedWorkNotes=$(echo "Associated Merge Request:" $GITLAB_ISSUE_LINK )
    - echo "updatedDescription:" $updatedDescription
    - echo "updatedShortDescription:" $updatedShortDescription

    - >
      curl -X PATCH
      -u $SK_SERVICENOW_USERNAME:$SK_SERVICENOW_PASSWORD
      $SK_CHANGE_API_USINGSYSID
      -H "Content-Type: application/json"
      -H "Accept: application/json"
      -d '{"work_notes": "'"${updatedWorkNotes//\"/\\\"}"'","short_description": "'"$updatedShortDescription"'", "description": "'"$updatedDescription"'"}'
  only:
    - main  

update_merge_request:
  stage: update_merge_request
  script:
    
    - echo "************************ update_merge_request Job Running ********************************************************************"
   
    - source artifacts/envariables.sh

    - echo "Import Change Details to combine with Merger Request details....."

    - echo "ChgSoDec:" $ChgSoDec
    - echo "ChgDec:" $ChgDec
    - echo "ChgSysID:" $ChgSysID
    - echo "ChgState:" $ChgState
    - echo "ChgNumber:" $ChgNumber

    - echo "Import Merger Request details......"

    - echo "MR_Title:" $MR_Title
    - echo "MR_ID:" $MR_ID
    - echo "MR_IID:" $MR_IID
    - echo "MR_State:" $MR_State
  
    - contentToUpdate=$(echo "ServiceNow Change Details:" "[${ChgNumber}](https://${SK_SERVICENOW_INSTANCE_FULL}/change_request.do?sys_id=${ChgSysID})" ",State :" $ChgState  ",Short Description:" $ChgSoDec )

    - |
      curl --request POST \
        --header "Content-Type: application/json" \
        --header "Authorization: Bearer $SK_CI_JOB_TOKEN" \
        --data "{\"body\":\"$contentToUpdate\"}" \
        "${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/merge_requests/${MR_IID}/notes"
  only:
    - main

  
wait_for_ChangeApproval_stateImplemented:      
  stage: wait_for_ChangeApproval_stateImplemented  

  before_script:
    - chmod +x ./fetch_snowDevOpsChange_details.sh

  script:
    - echo "************************ wait_for_ChangeApproval_stateImplemented Job Running ********************************************************************"
    - apt-get update -qy
    - apt-get install -y curl jq

    # Fetch the change state until its in implement state
    # Set the timeout (in seconds)
    
    - export TIMEOUT=60  # 1 minutes timeout
    - export CHANGE_STATE=""
    - export START_TIME=$(date +%s)
    - |
      while [ "$CHANGE_STATE" != "Implement" ]; do
      CURRENT_TIME=$(date +%s)
      ELAPSED_TIME=$((CURRENT_TIME - START_TIME))

      if [ "$ELAPSED_TIME" -ge "$TIMEOUT" ]; then
        echo "Timeout reached. Exiting the loop."
        break
      fi

      # Fetch the change Details
      changeDetails=$(./fetch_snowDevOpsChange_details.sh $CI_PIPELINE_ID)
      CHANGE_STATE=$(echo "$changeDetails" | jq -r '.result[0].state')
      ChangeApproval=$(echo "$changeDetails" | jq -r '.result[0].approval')
      echo $CHANGE_STATE
      echo $ChangeApproval
      sleep 10s
      done

      if [ "$CHANGE_STATE" = "Implement" ]; then
      echo "Change is now in Implement state. Proceeding with deployment."
      else
      echo "Timeout reached. Deployment aborted."
      exit 1
      fi
  only:
    - main
     

     
deploy:
    stage: deploy
    allow_failure: false
    script:

      - echo "************************ deploy Job Running ********************************************************************"
      - echo 'prod'
  

wait_forAll_jobCompletion:
  stage: wait_forAll_jobCompletion
  script:
    - echo "************************ wait_forAll_jobCompletion Job Running ********************************************************************"
    - echo "Sleeping for 80 seconds after all jobs in the pipeline"
    - sleep 80
  when: always

upload_logTo_serviceNow:
  stage: upload_logTo_serviceNow
  before_script:
    - chmod +x ./upload_attachment.sh
  script:
    - echo "************************ upload_logTo_serviceNow Job Running ********************************************************************"
    - source artifacts/envariables.sh
    - echo "Import Change Details to combine with Merger Request details....."
    - echo "ChgSoDec:" $ChgSoDec
    - echo "ChgDec:" $ChgDec
    - echo "ChgSysID:" $ChgSysID
    - echo "ChgState:" $ChgState
    - echo "ChgNumber:" $ChgNumber

    - echo "Import Merger Request details......"
    - echo "MR_Title:" $MR_Title
    - echo "MR_ID:" $MR_ID
    - echo "MR_IID:" $MR_IID
    - echo "MR_State:" $MR_State
    - mkdir -p trace_files
    - apt-get update -qy
    - apt-get install -y curl jq
    - API_BASE_URL="${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/jobs"
    - JOBS=$(curl --header "PRIVATE-TOKEN:"" $SK_CI_JOB_TOKEN" "$API_BASE_URL" | jq -r '.[].id')
    - echo "Iterate over jobs and download trace files....."
    - for JOB_ID in $JOBS; do
        API_URL="${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/jobs/$JOB_ID/trace";
        echo "Downloading trace file for Job ID:"" $JOB_ID";
        curl --location --header "PRIVATE-TOKEN:"" $SK_CI_JOB_TOKEN" "$API_URL" >> "trace_files/GitLabPipeline.${CI_PIPELINE_ID}.job.log";
      done
    - ls -la trace_files
    - ./upload_attachment.sh "${CI_PROJECT_DIR}/trace_files/GitLabPipeline.${CI_PIPELINE_ID}.job.log" "$ChgSysID"


```

### YAML File configuration V 4.0

```yaml


variables:
  GIT_SSL_NO_VERIFY: "true"

  SK_CI_JOB_TOKEN:
    description: "GitLab Access Token. Used to create or update comments on Merge Requests"
  SK_GITLAB_URL:
    description: "GitLab url used to create a API "
  SK_SERVICENOW_INSTANCE_FULL:
    description: "ServiceNow instance URL to form an API to connect with ServiceNow"
  SK_SERVICENOW_PASSWORD:
    description: "ServiceNow instance password to provide authentication"
  SK_SERVICENOW_USERNAME:
    description: "ServiceNow User Name for API authentication"
  
 

# List of stages for jobs, and their order of execution
stages:          
  - build
  - fetch_mergeRequest_details
  - snowAutomaticChangeCreation
  - fetch_snowDevOpsChange_details
  - update_snowDevOpsChange
  - update_merge_request
  - wait_for_ChangeApproval_stateImplemented
  - deploy
  - wait_forAll_jobCompletion
  - upload_logTo_serviceNow
  
  
build-job:       # This job runs in the build stage, which runs first.
  stage: build
  script:
    - echo "************************ build Job Running ********************************************************************"
    - echo "Compiling the code..."
    - echo "Compile complete."
  only:
    - main

fetch_mergeRequest_details:
  stage: fetch_mergeRequest_details
  script:
    - apt-get update -qy
    - apt-get install -y curl jq
   
    - echo "CI_COMMIT_SHA:" $CI_COMMIT_SHA
    - echo "CI_COMMIT_REF_NAME:" $CI_COMMIT_REF_NAME
    - echo "SOURCE_BRANCH:" $SOURCE_BRANCH  # Assuming that the source branch is the same as the commit reference name
    - echo "CI_PROJECT_ID:" $CI_PROJECT_ID
    - |
      # Set the GitLab API URL
      API_URL="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests"
      # Set the commit SHA from the CI environment variable
      COMMIT_SHA="$CI_COMMIT_SHA"
      # Make the API request to fetch merge request ID
      MR_IID=$(curl --header "PRIVATE-TOKEN: $SK_CI_JOB_TOKEN" "$API_URL" | jq --arg commit_sha "$COMMIT_SHA" '.[] | select(.merge_commit_sha == $commit_sha) | .iid')
      # Print the merge request ID
      echo "Merge Request IID: $MR_IID"
      
      
      # Check if MR_IID is empty
      if [ -z "$MR_IID" ]; then
        echo "There is no merge request associated with this commit. Exiting the pipeline. No change has been created in ServiceNow "
        exit 1
      fi
    
      MR_DETAILS=$(curl -s --request GET \
        --header "Content-Type: application/json" \
        --header "Authorization: Bearer $SK_CI_JOB_TOKEN" \
        "${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/merge_requests/${MR_IID}")
    
    - mkdir -p artifacts
    - echo "MRDetails "
    - MR_Title=$(echo $MR_DETAILS | jq -r '.title')
    - MR_ID=$(echo $MR_DETAILS | jq -r '.id')
    - MR_IID=$(echo $MR_DETAILS | jq -r '.iid')
    - MR_State=$(echo $MR_DETAILS | jq -r '.state')
    
    - echo "Merge Request Details:-" "Title:" $MR_Title  " Id:" $MR_ID " IID:" $MR_IID " State:" $MR_State
    
    - echo "Exporting variables to use in other jobs"
    
    - echo "export MR_Title=\"$MR_Title\"" >> artifacts/envariables.sh
    - echo "export MR_ID=\"$MR_ID\"" >> artifacts/envariables.sh
    - echo "export MR_IID=\"$MR_IID\"" >> artifacts/envariables.sh
    - echo "export MR_State=\"$MR_State\"" >> artifacts/envariables.sh
  artifacts:
    paths:
      - artifacts/
    # Only run this job on specific branches or events if needed
  only:
    - main

snowAutomaticChangeCreation:       # This job runs in the build stage, which runs first.
  stage: snowAutomaticChangeCreation
  script:
    - echo "************************ snowChangeCreation Job Running ********************************************************************"
    - echo "Change has been creted in ServiceNow..."
  only:
    - main

fetch_snowDevOpsChange_details:
  stage: fetch_snowDevOpsChange_details
  script:
    - echo "************************ fetch_snowDevOpsChange_details Job Running ********************************************************************"
    - apt-get update -qy
    - apt-get install -y curl jq
    - source artifacts/envariables.sh
    
    - echo "Merger Request Details"
    
    - echo "MR_Title:" $MR_Title
    - echo "MR_ID:" $MR_ID
    - echo "MR_IID:" $MR_IID
    - echo "MR_State:" $MR_State

  # API and code to fetch pipeline execution sysid rom ServiceNow
    - SK_API_PIPELINE_EXECUTION="https://${SK_SERVICENOW_INSTANCE_FULL}/api/now/table/sn_devops_pipeline_execution?sysparm_query=build_number=$CI_PIPELINE_ID&sysparm_fields=sys_id&sysparm_limit=1" 

    - echo "API for ServiceNow Pipeline Execution table:" $SK_API_PIPELINE_EXECUTION

        pipelineRresponse=$(curl -s -u "${SK_SERVICENOW_USERNAME}:${SK_SERVICENOW_PASSWORD}" "${SK_API_PIPELINE_EXECUTION}")

    - pipelineExecutionSysid=$(echo "$pipelineRresponse" | jq -r '.result[0].sys_id')
    - echo "pipelineExecutionSysid:" $pipelineExecutionSysid 
   
  
  # API and code to fetch Step execution sysid from ServiceNow
    - SK_API_STEP_EXECUTION="https://${SK_SERVICENOW_INSTANCE_FULL}/api/now/table/sn_devops_step_execution?sysparm_query=pipeline_execution.sys_id=${pipelineExecutionSysid}^change_requestISNOTEMPTY&sysparm_fields=change_request&sysparm_limit=1" 

    - echo "API for ServiceNow Step Execution table:" $SK_API_STEP_EXECUTION

       responseWithChgSysID=$(curl -s -u "${SK_SERVICENOW_USERNAME}:${SK_SERVICENOW_PASSWORD}" "${SK_API_STEP_EXECUTION}")

    - chngSysID=$(echo "$responseWithChgSysID" | jq -r '.result[0].change_request.value')

    - echo "chngSysID:" $chngSysID 

  # API and code to ServiceNow Change Details
    - SK_API_SNOW_CHANGE_DETAILS="https://dev183160.service-now.com/api/now/table/change_request?sysparm_query=sys_idSTARTSWITH${chngSysID}&sysparm_display_value=true"

    - echo "API for ServiceNow Change REquest table:" $SK_API_SNOW_CHANGE_DETAILS

       responseWithChgDetails=$(curl -s -u "${SK_SERVICENOW_USERNAME}:${SK_SERVICENOW_PASSWORD}" "${SK_API_SNOW_CHANGE_DETAILS}")

    - echo "responseWithChgDetails:" $responseWithChgDetails

  # Parse the ServiceNow Change details
    - ChgSoDec=$(echo $responseWithChgDetails | jq -r '.result[0].short_description')
    - ChgDec=$(echo $responseWithChgDetails | jq -r '.result[0].description')
    - ChgSysID=$(echo $responseWithChgDetails | jq -r '.result[0].sys_id')
    - ChgState=$(echo $responseWithChgDetails | jq -r '.result[0].state')
    - ChgNumber=$(echo $responseWithChgDetails | jq -r '.result[0].number')

    - echo "ServiceNow Change Details:" " ChgNumber:" $ChgNumber ", ChgState:" $ChgState ",ChgSoDec:" $ChgSoDec ", ChgSysID:" $ChgSysID 
   
    - echo "Exporting variables to use in other jobs"

    - echo "export MR_Title=\"$MR_Title\"" >> artifacts/envariables.sh
    - echo "export MR_ID=\"$MR_ID\"" >> artifacts/envariables.sh
    - echo "export MR_IID=\"$MR_IID\"" >> artifacts/envariables.sh
    - echo "export MR_State=\"$MR_State\"" >> artifacts/envariables.sh
    

    - echo "Exporting variables to use in other jobs"

    - echo "export ChgSoDec=\"$ChgSoDec\"" >> artifacts/envariables.sh
    - echo "export ChgDec=\"$ChgDec\"" >> artifacts/envariables.sh
    - echo "export ChgSysID=\"$ChgSysID\"" >> artifacts/envariables.sh
    - echo "export ChgState=\"$ChgState\"" >> artifacts/envariables.sh
    - echo "export ChgNumber=\"$ChgNumber\"" >> artifacts/envariables.sh
    
  artifacts:
    paths:
      - artifacts/
  only:
    - main
   
update_snowDevOpsChange:
  stage: update_snowDevOpsChange
  script:
    - echo "************************ update_snowDevOpsChange Job Running ********************************************************************"
    - source artifacts/envariables.sh
    - echo "Import Change Details to combine with Merger Request details....."
    - echo "ChgSoDec:" $ChgSoDec
    - echo "ChgDec:" $ChgDec
    - echo "ChgSysID:" $ChgSysID
    - echo "ChgState:" $ChgState
    - echo "ChgNumber:" $ChgNumber

    - echo "Import Merger Request details......"
    - echo "MR_Title:" $MR_Title
    - echo "MR_ID:" $MR_ID
    - echo "MR_IID:" $MR_IID
    - echo "MR_State:" $MR_State
    
    #Form a gitLab Merge Request URL
    - PROJECT_URL="https://gitlab.com/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME"
    - echo "Project URL:"" $PROJECT_URL"
    - GITLAB_ISSUE_LINK=$(echo "[code]<a href=\"$PROJECT_URL/merge_requests/$MR_IID\"target="_blank">Link to GitLab Merge Request</a>[/code]")
    
    - SK_CHANGE_API_USINGSYSID="https://${SK_SERVICENOW_INSTANCE_FULL}/api/now/table/change_request/$ChgSysID"
    - echo "SK_CHANGE_API_USINGSYSID:" $SK_CHANGE_API_USINGSYSID

    - updatedShortDescription=$(echo $ChgSoDec ", MR_Title:" $MR_Title ", MR_ID:" $MR_ID ", MR_IID:" $MR_IID)
    - updatedDescription=$(echo $ChgDec ", MR_Title:" $MR_Title ", MR_ID:" $MR_ID ", MR_IID:" $MR_IID)
    - updatedWorkNotes=$(echo "Associated Merge Request Details:" $GITLAB_ISSUE_LINK ", MR_Title:" $MR_Title ", MR_ID:" $MR_ID ", MR_IID:" $MR_IID)
    #- updatedWorkNotes=$(echo "Associated Merge Request:" $GITLAB_ISSUE_LINK )
    - echo "updatedDescription:" $updatedDescription
    - echo "updatedShortDescription:" $updatedShortDescription

    - >
      curl -X PATCH
      -u $SK_SERVICENOW_USERNAME:$SK_SERVICENOW_PASSWORD
      $SK_CHANGE_API_USINGSYSID
      -H "Content-Type: application/json"
      -H "Accept: application/json"
      -d '{"work_notes": "'"${updatedWorkNotes//\"/\\\"}"'","short_description": "'"$updatedShortDescription"'", "description": "'"$updatedDescription"'"}'
  only:
    - main  

update_merge_request:
  stage: update_merge_request
  script:
    
    - echo "************************ update_merge_request Job Running ********************************************************************"
   
    - source artifacts/envariables.sh

    - echo "Import Change Details to combine with Merger Request details....."

    - echo "ChgSoDec:" $ChgSoDec
    - echo "ChgDec:" $ChgDec
    - echo "ChgSysID:" $ChgSysID
    - echo "ChgState:" $ChgState
    - echo "ChgNumber:" $ChgNumber

    - echo "Import Merger Request details......"

    - echo "MR_Title:" $MR_Title
    - echo "MR_ID:" $MR_ID
    - echo "MR_IID:" $MR_IID
    - echo "MR_State:" $MR_State
  
    - contentToUpdate=$(echo "ServiceNow Change Details:" "[${ChgNumber}](https://${SK_SERVICENOW_INSTANCE_FULL}/change_request.do?sys_id=${ChgSysID})" ",State :" $ChgState  ",Short Description:" $ChgSoDec )

    - |
      curl --request POST \
        --header "Content-Type: application/json" \
        --header "Authorization: Bearer $SK_CI_JOB_TOKEN" \
        --data "{\"body\":\"$contentToUpdate\"}" \
        "${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/merge_requests/${MR_IID}/notes"
  only:
    - main

  
wait_for_ChangeApproval_stateImplemented:      
  stage: wait_for_ChangeApproval_stateImplemented  

  before_script:
    - chmod +x ./fetch_snowDevOpsChange_details.sh

  script:
    - echo "************************ wait_for_ChangeApproval_stateImplemented Job Running ********************************************************************"
    - apt-get update -qy
    - apt-get install -y curl jq

    # Fetch the change state until its in implement state
    # Set the timeout (in seconds)
    
    - export TIMEOUT=60  # 1 minutes timeout
    - export CHANGE_STATE=""
    - export START_TIME=$(date +%s)
    - |
      while [ "$CHANGE_STATE" != "Implement" ]; do
      CURRENT_TIME=$(date +%s)
      ELAPSED_TIME=$((CURRENT_TIME - START_TIME))

      if [ "$ELAPSED_TIME" -ge "$TIMEOUT" ]; then
        echo "Timeout reached. Exiting the loop."
        break
      fi

      # Fetch the change Details
      changeDetails=$(./fetch_snowDevOpsChange_details.sh $CI_PIPELINE_ID)
      CHANGE_STATE=$(echo "$changeDetails" | jq -r '.result[0].state')
      ChangeApproval=$(echo "$changeDetails" | jq -r '.result[0].approval')
      echo $CHANGE_STATE
      echo $ChangeApproval
      sleep 10s
      done

      if [ "$CHANGE_STATE" = "Implement" ]; then
      echo "Change is now in Implement state. Proceeding with deployment."
      else
      echo "Timeout reached. Deployment aborted."
      exit 1
      fi
  only:
    - main
     

     
deploy:
    stage: deploy
    #when: manual
    allow_failure: false
    script:

      - echo "************************ deploy Job Running ********************************************************************"
      - echo 'prod'
    only:
    - main

wait_forAll_jobCompletion:
  stage: wait_forAll_jobCompletion
  script:
    - echo "************************ wait_forAll_jobCompletion Job Running ********************************************************************"
    - echo "Sleeping for 80 seconds after all jobs in the pipeline"
    - sleep 20
  #when: always
  only:
    - main

upload_logTo_serviceNow:
  stage: upload_logTo_serviceNow
  before_script:
    - chmod +x ./upload_attachment.sh
  script:
    - echo "************************ upload_logTo_serviceNow Job Running ********************************************************************"
    - source artifacts/envariables.sh
    - echo "Import Change Details to combine with Merger Request details....."
    - echo "ChgSoDec:" $ChgSoDec
    - echo "ChgDec:" $ChgDec
    - echo "ChgSysID:" $ChgSysID
    
    - echo "ChgState:" $ChgState
    - echo "ChgNumber:" $ChgNumber

    - echo "Import Merger Request details......"
    - echo "MR_Title:" $MR_Title
    - echo "MR_ID:" $MR_ID
    - echo "MR_IID:" $MR_IID
    - echo "MR_State:" $MR_State
    - mkdir -p trace_files
    - apt-get update -qy
    - apt-get install -y curl jq
    - API_BASE_URL="${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/jobs"
    - JOBS=$(curl --header "PRIVATE-TOKEN:"" $SK_CI_JOB_TOKEN" "$API_BASE_URL" | jq -r '.[].id')
    - echo "Iterate over jobs and download trace files....."
    - for JOB_ID in $JOBS; do
        API_URL="${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/jobs/$JOB_ID/trace";
        echo "Downloading trace file for Job ID:"" $JOB_ID";
        curl --location --header "PRIVATE-TOKEN:"" $SK_CI_JOB_TOKEN" "$API_URL" >> "trace_files/GitLabPipeline.${CI_PIPELINE_ID}.job.log";
      done
    - ls -la trace_files
    - ./upload_attachment.sh "${CI_PROJECT_DIR}/trace_files/GitLabPipeline.${CI_PIPELINE_ID}.job.log" "$ChgSysID"
  
  only:
    - main


```

###  YAML File configuration V 5.0

```yaml

variables:
  GIT_SSL_NO_VERIFY: "true"

  SK_CI_JOB_TOKEN:
    description: "GitLab Access Token. Used to create or update comments on Merge Requests"
  SK_GITLAB_URL:
    description: "GitLab url used to create a API "
  SK_SERVICENOW_INSTANCE_FULL:
    description: "ServiceNow instance URL to form an API to connect with ServiceNow"
  SK_SERVICENOW_PASSWORD:
    description: "ServiceNow instance password to provide authentication"
  SK_SERVICENOW_USERNAME:
    description: "ServiceNow User Name for API authentication"
  
 

# #List of stages for jobs, and their order of execution
stages:          
  - build
  - fetch_mergeRequest_details
  - createChange_and_WaitForApproval
  - fetch_snowDevOpsChange_details
  - update_snowDevOpsChange
  - update_merge_request
  - deploy
  - integration_test
  - wait_forAll_jobCompletion
  - upload_logTo_serviceNow
  
  
build-job:       # This job runs in the build stage, which runs first.
  stage: build
  script:
    - echo "************************ build Job Running ********************************************************************"
    - echo "Compiling the code..."
    - echo "Compile complete."
  only:
    - main

fetch_mergeRequest_details:
  stage: fetch_mergeRequest_details
  script:
    - apt-get update -qy
    - apt-get install -y curl jq
   
    - echo "CI_COMMIT_SHA:" $CI_COMMIT_SHA
    - echo "CI_COMMIT_REF_NAME:" $CI_COMMIT_REF_NAME
    - echo "SOURCE_BRANCH:" $SOURCE_BRANCH  # Assuming that the source branch is the same as the commit reference name
    - echo "CI_PROJECT_ID:" $CI_PROJECT_ID
    - |
      # Set the GitLab API URL
      API_URL="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests"
      # Set the commit SHA from the CI environment variable
      COMMIT_SHA="$CI_COMMIT_SHA"
      # Make the API request to fetch merge request ID
      MR_IID=$(curl --header "PRIVATE-TOKEN: $SK_CI_JOB_TOKEN" "$API_URL" | jq --arg commit_sha "$COMMIT_SHA" '.[] | select(.merge_commit_sha == $commit_sha) | .iid')
      # Print the merge request ID
      echo "Merge Request IID: $MR_IID"
      
      
      # Check if MR_IID is empty
      if [ -z "$MR_IID" ]; then
        echo "There is no merge request associated with this commit. Exiting the pipeline. No change has been created in ServiceNow "
        exit 1
      fi
    
      MR_DETAILS=$(curl -s --request GET \
        --header "Content-Type: application/json" \
        --header "Authorization: Bearer $SK_CI_JOB_TOKEN" \
        "${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/merge_requests/${MR_IID}")
    
    - mkdir -p artifacts
    - echo "MRDetails "
    - MR_Title=$(echo $MR_DETAILS | jq -r '.title')
    - MR_ID=$(echo $MR_DETAILS | jq -r '.id')
    - MR_IID=$(echo $MR_DETAILS | jq -r '.iid')
    - MR_State=$(echo $MR_DETAILS | jq -r '.state')
    
    - echo "Merge Request Details:-" "Title:" $MR_Title  " Id:" $MR_ID " IID:" $MR_IID " State:" $MR_State
    
    - echo "Exporting variables to use in other jobs"
    
    - echo "export MR_Title=\"$MR_Title\"" >> artifacts/envariables.sh
    - echo "export MR_ID=\"$MR_ID\"" >> artifacts/envariables.sh
    - echo "export MR_IID=\"$MR_IID\"" >> artifacts/envariables.sh
    - echo "export MR_State=\"$MR_State\"" >> artifacts/envariables.sh
  artifacts:
    paths:
      - artifacts/
    # Only run this job on specific branches or events if needed
  only:
    - main

createChange_and_WaitForApproval:       # This job runs in the build stage, which runs first.
  stage: createChange_and_WaitForApproval
  when: manual
  allow_failure: false
  script:
    - echo "************************ snowChangeCreation Job Running ********************************************************************"
    - echo "Change has been creted. Change is approved and in implement state, proceeding with change..."
  

  only:
    - main

fetch_snowDevOpsChange_details:
  stage: fetch_snowDevOpsChange_details
  script:
    - chmod +x updateMRandChangeWhenJobFails.sh
    - echo "************************ fetch_snowDevOpsChange_details Job Running ********************************************************************"
    - apt-get update -qy
    - apt-get install -y curl jq
    - source artifacts/envariables.sh
    
    - echo "Merger Request Details"
    
    - echo "MR_Title:" $MR_Title
    - echo "MR_ID:" $MR_ID
    - echo "MR_IID:" $MR_IID
    - echo "MR_State:" $MR_State

  # API and code to fetch pipeline execution sysid rom ServiceNow
    - SK_API_PIPELINE_EXECUTION="https://${SK_SERVICENOW_INSTANCE_FULL}/api/now/table/sn_devops_pipeline_execution?sysparm_query=build_number=$CI_PIPELINE_ID&sysparm_fields=sys_id&sysparm_limit=1" 

    - echo "API for ServiceNow Pipeline Execution table:" $SK_API_PIPELINE_EXECUTION

        pipelineRresponse=$(curl -s -u "${SK_SERVICENOW_USERNAME}:${SK_SERVICENOW_PASSWORD}" "${SK_API_PIPELINE_EXECUTION}")

    - pipelineExecutionSysid=$(echo "$pipelineRresponse" | jq -r '.result[0].sys_id')
    - echo "pipelineExecutionSysid:" $pipelineExecutionSysid 
   
  
  # API and code to fetch Step execution sysid from ServiceNow
    - SK_API_STEP_EXECUTION="https://${SK_SERVICENOW_INSTANCE_FULL}/api/now/table/sn_devops_step_execution?sysparm_query=pipeline_execution.sys_id=${pipelineExecutionSysid}^change_requestISNOTEMPTY&sysparm_fields=change_request&sysparm_limit=1" 

    - echo "API for ServiceNow Step Execution table:" $SK_API_STEP_EXECUTION

       responseWithChgSysID=$(curl -s -u "${SK_SERVICENOW_USERNAME}:${SK_SERVICENOW_PASSWORD}" "${SK_API_STEP_EXECUTION}")

    - chngSysID=$(echo "$responseWithChgSysID" | jq -r '.result[0].change_request.value')

    - echo "chngSysID:" $chngSysID 

  # API and code to ServiceNow Change Details
    - SK_API_SNOW_CHANGE_DETAILS="https://dev183160.service-now.com/api/now/table/change_request?sysparm_query=sys_idSTARTSWITH${chngSysID}&sysparm_display_value=true"

    - echo "API for ServiceNow Change REquest table:" $SK_API_SNOW_CHANGE_DETAILS

       responseWithChgDetails=$(curl -s -u "${SK_SERVICENOW_USERNAME}:${SK_SERVICENOW_PASSWORD}" "${SK_API_SNOW_CHANGE_DETAILS}")

    - echo "responseWithChgDetails:" $responseWithChgDetails

  # Parse the ServiceNow Change details
    - ChgSoDec=$(echo $responseWithChgDetails | jq -r '.result[0].short_description')
    - ChgDec=$(echo $responseWithChgDetails | jq -r '.result[0].description')
    - ChgSysID=$(echo $responseWithChgDetails | jq -r '.result[0].sys_id')
    - ChgState=$(echo $responseWithChgDetails | jq -r '.result[0].state')
    - ChgNumber=$(echo $responseWithChgDetails | jq -r '.result[0].number')

    - echo "ServiceNow Change Details:" " ChgNumber:" $ChgNumber ", ChgState:" $ChgState ",ChgSoDec:" $ChgSoDec ", ChgSysID:" $ChgSysID 
   
    - echo "Exporting variables to use in other jobs"

    - echo "export MR_Title=\"$MR_Title\"" >> artifacts/envariables.sh
    - echo "export MR_ID=\"$MR_ID\"" >> artifacts/envariables.sh
    - echo "export MR_IID=\"$MR_IID\"" >> artifacts/envariables.sh
    - echo "export MR_State=\"$MR_State\"" >> artifacts/envariables.sh
    

    - echo "Exporting variables to use in other jobs"

    - echo "export ChgSoDec=\"$ChgSoDec\"" >> artifacts/envariables.sh
    - echo "export ChgDec=\"$ChgDec\"" >> artifacts/envariables.sh
    - echo "export ChgSysID=\"$ChgSysID\"" >> artifacts/envariables.sh
    - echo "export ChgState=\"$ChgState\"" >> artifacts/envariables.sh
    - echo "export ChgNumber=\"$ChgNumber\"" >> artifacts/envariables.sh

  after_script:
    - source artifacts/envariables.sh
    - ./updateMRandChangeWhenJobFails.sh $MR_IID $ChgSysID
  artifacts:
    paths:
      - artifacts/
  only:
    - main
   
update_snowDevOpsChange:
  stage: update_snowDevOpsChange
  script:
    - chmod +x updateMRandChangeWhenJobFails.sh
    - echo "************************ update_snowDevOpsChange Job Running ********************************************************************"
    - source artifacts/envariables.sh
    - echo "Import Change Details to combine with Merger Request details....."
    - echo "ChgSoDec:" $ChgSoDec
    - echo "ChgDec:" $ChgDec
    - echo "ChgSysID:" $ChgSysID
    - echo "ChgState:" $ChgState
    - echo "ChgNumber:" $ChgNumber

    - echo "Import Merger Request details......"
    - echo "MR_Title:" $MR_Title
    - echo "MR_ID:" $MR_ID
    - echo "MR_IID:" $MR_IID
    - echo "MR_State:" $MR_State
    
    #Form a gitLab Merge Request URL
    - PROJECT_URL="https://gitlab.com/$CI_PROJECT_NAMESPACE/$CI_PROJECT_NAME"
    - echo "Project URL:"" $PROJECT_URL"
    - GITLAB_ISSUE_LINK=$(echo "[code]<a href=\"$PROJECT_URL/merge_requests/$MR_IID\"target="_blank">Link to GitLab Merge Request</a>[/code]")
    
    - SK_CHANGE_API_USINGSYSID="https://${SK_SERVICENOW_INSTANCE_FULL}/api/now/table/change_request/$ChgSysID"
    - echo "SK_CHANGE_API_USINGSYSID:" $SK_CHANGE_API_USINGSYSID

    - updatedShortDescription=$(echo $ChgSoDec ", MR_Title:" $MR_Title ", MR_ID:" $MR_ID ", MR_IID:" $MR_IID)
    - updatedDescription=$(echo $ChgDec ", MR_Title:" $MR_Title ", MR_ID:" $MR_ID ", MR_IID:" $MR_IID)
    - updatedWorkNotes=$(echo "Associated Merge Request Details:" $GITLAB_ISSUE_LINK ", MR_Title:" $MR_Title ", MR_ID:" $MR_ID ", MR_IID:" $MR_IID)
    #- updatedWorkNotes=$(echo "Associated Merge Request:" $GITLAB_ISSUE_LINK )
    - echo "updatedDescription:" $updatedDescription
    - echo "updatedShortDescription:" $updatedShortDescription

    - >
      curl -X PATCH
      -u $SK_SERVICENOW_USERNAME:$SK_SERVICENOW_PASSWORD
      $SK_CHANGE_API_USINGSYSID
      -H "Content-Type: application/json"
      -H "Accept: application/json"
      -d '{"work_notes": "'"${updatedWorkNotes//\"/\\\"}"'","short_description": "'"$updatedShortDescription"'", "description": "'"$updatedDescription"'"}'
  after_script:
    - source artifacts/envariables.sh
    - ./updateMRandChangeWhenJobFails.sh $MR_IID $ChgSysID
  only:
    - main  

update_merge_request:
  stage: update_merge_request
  script:
    - chmod +x updateMRandChangeWhenJobFails.sh
    - echo "************************ update_merge_request Job Running ********************************************************************"
   
    - source artifacts/envariables.sh

    - echo "Import Change Details to combine with Merger Request details....."

    - echo "ChgSoDec:" $ChgSoDec
    - echo "ChgDec:" $ChgDec
    - echo "ChgSysID:" $ChgSysID
    - echo "ChgState:" $ChgState
    - echo "ChgNumber:" $ChgNumber

    - echo "Import Merger Request details......"

    - echo "MR_Title:" $MR_Title
    - echo "MR_ID:" $MR_ID
    - echo "MR_IID:" $MR_IID
    - echo "MR_State:" $MR_State
  
    - contentToUpdate=$(echo "ServiceNow Change Details:" "[${ChgNumber}](https://${SK_SERVICENOW_INSTANCE_FULL}/change_request.do?sys_id=${ChgSysID})" ",State :" $ChgState  ",Short Description:" $ChgSoDec )

    - |
      curl --request POST \
        --header "Content-Type: application/json" \
        --header "Authorization: Bearer $SK_CI_JOB_TOKEN" \
        --data "{\"body\":\"$contentToUpdate\"}" \
        "${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/merge_requests/${MR_IID}/notes"
  after_script:
    - source artifacts/envariables.sh
    - ./updateMRandChangeWhenJobFails.sh $MR_IID $ChgSysID
  only:
    - main

  
deploy:
    stage: deploy
    script:
      - chmod +x updateMRandChangeWhenJobFails.sh
      - echo "************************ deploy Job Running ********************************************************************"
      - echo 'prod'
    after_script:
    - source artifacts/envariables.sh
    - ./updateMRandChangeWhenJobFails.sh $MR_IID $ChgSysID

    only:
    - main
integration_test:
  stage: integration_test
  script:
    - chmod +x updateMRandChangeWhenJobFails.sh
     - echo "TEST_JOB_ID=$CI_JOB_ID" >> build.env
     - ls -l 
     - chmod +x testcase_script.sh 
     - ./testcase_script.sh --output junit > test_comments.xml
  after_script:
    - source artifacts/envariables.sh
    - ./updateMRandChangeWhenJobFails.sh $MR_IID $ChgSysID
  artifacts:
    when: always
    reports:
      junit: test_cases.xml
      dotenv: build.env
  only:
    - main

wait_forAll_jobCompletion:
  stage: wait_forAll_jobCompletion
  script:
    - chmod +x updateMRandChangeWhenJobFails.sh
    - echo "************************ wait_forAll_jobCompletion Job Running ********************************************************************"
    - echo "Sleeping for 80 seconds after all jobs in the pipeline"
    - sleep 20
  after_script:
    - source artifacts/envariables.sh
    - ./updateMRandChangeWhenJobFails.sh $MR_IID $ChgSysID
  only:
    - main

upload_logTo_serviceNow:
  stage: upload_logTo_serviceNow
  before_script:
    - chmod +x ./upload_attachment.sh
    - chmod +x updateMRandChangeWhenJobFails.sh
  script:
    - echo "************************ upload_logTo_serviceNow Job Running ********************************************************************"
    - source artifacts/envariables.sh
    - echo "Import Change Details to combine with Merger Request details....."
    - echo "ChgSoDec:" $ChgSoDec
    - echo "ChgDec:" $ChgDec
    - echo "ChgSysID:" $ChgSysID
    
    - echo "ChgState:" $ChgState
    - echo "ChgNumber:" $ChgNumber

    - echo "Import Merger Request details......"
    - echo "MR_Title:" $MR_Title
    - echo "MR_ID:" $MR_ID
    - echo "MR_IID:" $MR_IID
    - echo "MR_State:" $MR_State
    - mkdir -p trace_files
    - apt-get update -qy
    - apt-get install -y curl jq
    - API_BASE_URL="${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/jobs"
    - JOBS=$(curl --header "PRIVATE-TOKEN:"" $SK_CI_JOB_TOKEN" "$API_BASE_URL" | jq -r '.[].id')
    - echo "Iterate over jobs and download trace files....."
    - for JOB_ID in $JOBS; do
        API_URL="${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/jobs/$JOB_ID/trace";
        echo "Downloading trace file for Job ID:"" $JOB_ID";
        curl --location --header "PRIVATE-TOKEN:"" $SK_CI_JOB_TOKEN" "$API_URL" >> "trace_files/GitLabPipeline.${CI_PIPELINE_ID}.job.log";
      done
    - ls -la trace_files
    - ./upload_attachment.sh "${CI_PROJECT_DIR}/trace_files/GitLabPipeline.${CI_PIPELINE_ID}.job.log" "$ChgSysID"
  
  when: always
  after_script:
    - source artifacts/envariables.sh
    - ./updateMRandChangeWhenJobFails.sh $MR_IID $ChgSysID
  only:
    - main



```
## Shell Script used in CI/CD pipeline

### Shell script to update merge request
```shell

#!/bin/bash

# GitLab API endpoint for updating a merge request
GITLAB_URL="https://gitlab.com"
PROJECT_ID=$1
MERGE_REQUEST_IID=$2
contentToUpdate=$3

GITLAB_API="${GITLAB_URL}/api/v4/projects/${PROJECT_ID}/merge_requests/${MERGE_REQUEST_IID}"


ACCESS_TOKEN=$SK_CI_JOB_TOKEN
NEW_DESCRIPTION="NEW_DESCRIPTION"

# cURL command to update the merge request
curl --request PUT \
  --url "${GITLAB_API}" \
  --header "PRIVATE-TOKEN: ${ACCESS_TOKEN}" \
  --data "{
    \"note\": \"${contentToUpdate}\",
    \"description\": \"${NEW_DESCRIPTION}\"
  }"



```
### Shell script to update ServiceNow DevOps Change
```shell

#!/bin/bash

SERVICE_NOW_INSTANCE=$SK_SERVICENOW_INSTANCE_FULL
USERNAME=$SK_SERVICENOW_USERNAME
PASSWORD=$SK_SERVICENOW_PASSWORD

CHANGE_REQUEST_ID=$1
updatedShortDescription=$2


# API EndPoint 
SERVICE_NOW_API="https://${SERVICE_NOW_INSTANCE}/api/now/table/change_request/${CHANGE_REQUEST_ID}"


# cURL command to update the change request
curl --request PUT \
  --url "${SERVICE_NOW_API}" \
  --header "Accept: application/json" \
  --header "Content-Type: application/json" \
  --user "${USERNAME}:${PASSWORD}" \
  --data "{
    \"short_description\": \"${updatedShortDescription}\",
    \"description\": \"${updatedShortDescription}\"
  }"


```
### Shell script to fetch merge request
```shell

#!/bin/bash


PROJECT_ID=$1
GITLAB_API_URL='https://gitlab.com/api/v4'
ACCESS_TOKEN=$SK_CI_JOB_TOKEN

# Fetch Merge Request Details.
MERGE_REQUEST_DETAILS=$(curl --header "PRIVATE-TOKEN: ${ACCESS_TOKEN}" "${GITLAB_API_URL}/projects/${PROJECT_ID}/merge_requests")

# Print Merge Request Details
echo "$MERGE_REQUEST_DETAILS"


```
### Shell script to fetch ServiceNow DevOps Change
```shell

#!/bin/bash


PipelineID=$1
SERVICE_NOW_INSTANCE=$SK_SERVICENOW_INSTANCE_FULL
USERNAME=$SK_SERVICENOW_USERNAME
PASSWORD=$SK_SERVICENOW_PASSWORD

# Fetch short_description from ServiceNow
response=$(curl -s -u "$USERNAME:$PASSWORD" "https://$SERVICE_NOW_INSTANCE/api/now/table/change_request?sysparm_query=implementation_planLIKE${PipelineID}&sysparm_display_value=true&sysparm_limit=1")

echo "$response"


```
### Shell script to upload attachment in ServiceNow DevOps Change
```shell

#!/bin/bash

SN_INSTANCE=$SK_SERVICENOW_INSTANCE_FULL
SN_USERNAME=$SK_SERVICENOW_USERNAME
SN_PASSWORD=$SK_SERVICENOW_PASSWORD

LOG_FILE_PATH=$1
CHANGE_REQUEST_SYS_ID=$2

# Check if the log file path is provided
if [ -z "$LOG_FILE_PATH" ]; then
  echo "Error: Log file path not provided."
  exit 1
fi

LOG_FILE_NAME=$(basename "$LOG_FILE_PATH")

# ServiceNow API URL for attachments
SN_ATTACHMENT_API="https://${SN_INSTANCE}/api/now/attachment/file?table_name=change_request&table_sys_id=${CHANGE_REQUEST_SYS_ID}&file_name=${LOG_FILE_NAME}"

# Upload the attachment using curl
curl -X POST \
  -u "${SN_USERNAME}:${SN_PASSWORD}" \
  -H "Content-Type: text/html" \
  -F "file=@${LOG_FILE_PATH}" \
  "$SN_ATTACHMENT_API"



```
### Shell script to update the comments in ServiceNow and Merge Request when Pipeline fails 

```shell

#!/bin/bash

# Set variables
MR_ID=$1
PROJECT_ID=$CI_PROJECT_ID
ACCESS_TOKEN=$SK_CI_JOB_TOKEN
SERVICE_NOW_INSTANCE=$SK_SERVICENOW_INSTANCE_FULL
USERNAME=$SK_SERVICENOW_USERNAME
PASSWORD=$SK_SERVICENOW_PASSWORD
CHANGE_REQUEST_ID=$2

# Check if the job failed
if [ "$CI_JOB_STATUS" == "failed" ]; then
  # Add a comment to the merge request
  COMMENT=":x: CI/CD Job Failed: Job ($CI_JOB_NAME) failed! JOB ID $CI_JOB_ID ."
  curl --request POST --header "PRIVATE-TOKEN: $ACCESS_TOKEN" --data "body=$COMMENT" "https://gitlab.com/api/v4/projects/$PROJECT_ID/merge_requests/$MR_ID/notes"

  # Update comments in the ServiceNow Change 
  COMMENT_MESSAGE="❌  GitLab CI/CD Pipeline Failed: Job ($CI_JOB_NAME) failed! JOB ID $CI_JOB_ID "
  SERVICE_NOW_API="https://${SERVICE_NOW_INSTANCE}/api/now/table/change_request/$CHANGE_REQUEST_ID"

  curl --request PUT \
    --url "${SERVICE_NOW_API}" \
    --user "${USERNAME}:${PASSWORD}" \
    --header "Accept: application/json" \
    --header "Content-Type: application/json" \
    --data "{
      \"comments\": \"$COMMENT_MESSAGE\"
    }"
fi




```

### YAML script sample to upload pipeline logs as attachment in ServiceNow DevOps Change

```yaml

stages:
  - upload_logTo_serviceNow

upload_logTo_serviceNow:
  stage: upload_logTo_serviceNow
  before_script:
    - chmod +x ./upload_attachment.sh
  script:
    - mkdir -p trace_files
    - apt-get update -qy
    - apt-get install -y curl jq
    - API_BASE_URL="${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/pipelines/${CI_PIPELINE_ID}/jobs"
    - JOBS=$(curl --header "PRIVATE-TOKEN:"" $SK_CI_JOB_TOKEN" "$API_BASE_URL" | jq -r '.[].id')
    # Iterate over jobs and download trace files
    - for JOB_ID in $JOBS; do
        API_URL="${SK_GITLAB_URL}/projects/${CI_PROJECT_ID}/jobs/$JOB_ID/trace";
        echo "Downloading trace file for Job ID:"" $JOB_ID";
        curl --location --header "PRIVATE-TOKEN:"" $SK_CI_JOB_TOKEN" "$API_URL" >> "trace_files/${CI_PIPELINE_ID}.job.log";
      done
    - ls -la trace_files
    - ./upload_attachment.sh "${CI_PROJECT_DIR}/trace_files/${CI_PIPELINE_ID}.job.log"


```
# ServiceNow DevOps Change Velocity

DevOps Change Velocity integrates with an organization\'s DevOps
toolchain to bring together data into one single location. Any time
there is a code change, regardless of where the change comes from (i.e.,
defect, enhancement, story, hotfix/patch, etc), the system tracks the
changes, builds, tests, etc.
  
Then, when enabled, Change Management takes the information from the
DevOps toolchain and automatically creates the DevOps Change Request.

DevOps Change Velocity, the preferred project management method is
Agile. DevOps is the natural implementation of the Agile method. Agile's
main focus is speed, while DevOps is accuracy. Combining both
successfully results in the best of both worlds, where teams rapidly
build, test, and deploy updates that are stable.  

Note: ServiceNow DevOps currently supports the following tool
integrations out of the box:

1. Planning: Azure Boards, Jira, ServiceNow Agile Development 2.0,
    Rally

2. Coding: Azure Repos, GitHub, GitHub Enterprise, GitLab, Bitbucket 

3. Orchestration: Azure Pipelines, Jenkins, GitLab, GitHub Enterprise

4. Repository Artifacts: JFrog, Azure Artifacts

5. Testing: GitHub, GitHub Enterprise, GitLab, Azure DevOps, Jenkins

6. Software Quality: SonarQube for Azure DevOps and Jenkins

7. Feature Flag: Split

### Key Features

**Change Acceleration**

Automatically create Change Requests (CHG) for stages under change
control. The DevOps Change Workflow and DevOps Change Approval policy
enable you to automate change approvals. Completely automate the change
approval process by using data from the toolchain and data that is
already in the ServiceNow platform to match sophisticated change
policies.

**Pipeline Modeling**

Harmonize and model your Azure DevOps, GitLab, Jenkins, and other DevOps
pipelines in the ServiceNow platform for simpler process management and
end-to-end visibility that is unique to ServiceNow DevOps.

Integrations

Integrations to your existing DevOps toolchain collect lifecycle events
and data, saving time and integration maintenance expenses. Several
base-system integrations like Jenkins and Azure DevOps require the
DevOps Integrations app, which is included in the ServiceNow DevOps
license. Our integration packages simplify onboarding (connecting to)
the following tools:

 

Base-system integrations:

-   Planning: Jira, ITBM Agile 2.0, Azure DevOps Boards

-   Code: GitHub.com, Enterprise GitHub, Bitbucket, Azure DevOps Repos

-   Orchestration: Jenkins, Azure DevOps Pipelines, GitLab via our
    DevOps Integrations app

**DevOps Insights**

DevOps Insights surfaces shared insights through reporting and
analytics, including the four key \'Accelerate\' metrics. The resulting
operational and business insights significantly improve collaboration
and communication. 

### Personas

Benefits of DevOps Change Velocity per persona
| Persona | Challenges | DevOps Change Velocity benefits |
|---|---|---|
| Developer | Long feedback loop for change approval Manual collection of data Swivel chair from changes to work items Lack of end-to-end visibility of entire DevOps lifecycle Significant time spent on admin tasks | Automated data collection process through integrations No time lost on manually creating a change request or providing evidence manually More time for actual development work Developers can keep working in their own tools |
| Change manager | Difficulty in handling increasing number of changes Not able to focus on only the changes that truly need human intervention Manual work to respond to audit request Significant time spent on gathering data | Accelerate change process with automated flows Visibility over pipeline, tests, and change in a single tool Automated documentation of change content and controls Automate appropriate change governance driven by data One-click audit response |
| Release manager | Lack of correlation between changes and release to measure progress Unable to eliminate manual tasks from delivery processes| Lack of correlation between changes and release to measure progress Unable to eliminate manual tasks from delivery processes|
| Application development leadership| Lack of visibility across teams and value streams Lack of team performance comparison data Inability to identify and resolve performance bottlenecks| Visibility into the entire value stream Normalized views of team performance Identify bottlenecks and take appropriate action|
| Operations and Support teams | Limited forecast and speed of remediation Lack of visibility over change content and impact Too many tools to correlate change, release, and incidents| Single tool to manage changes and incidents Easier correlation between change and incident Reduced MTTR from improved visibility|
|IT compliance officer | Limited visibility to ensure compliance Lack of transparency of data and process to measurerisks | Limited visibility to ensure compliance Lack of transparency of data and process to measurerisks |



## Requirement

DevOps Change Velocity supports both Code (Repository) and Orchestration
(Pipelines) capabilities for the GitLab tool. In case of pipelines, only
basic pipelines are supported but not the multi-project ones.

- Connect: Discover repositories and pipeline definitions by
    connecting your GitLab instances to DevOps Change Velocity.

- Configure: Enable sending real-time notifications for commits and
    pipelines by automatically creating a Webhook in GitLab so that this
    data can be used to create change policies.

We need to have below points in mid -

- It requires ITSM Pro subscription.

    a.  It Include DevOps change velocity and DevOps Config

        i.  We can install these using.

            1.  ServiceNow Store

            2.  ServiceNow Plugins module

    b.  One time Setup

        i.  Two users are required for this integration : By Default, it
            will be created in ServiceNow once we enable the plugins.

            1.  devops.integration.user

                a.  sn_devops.integration

            2.  devops.system

                a.  Role : sn_devops.admin

        ii. We need to setup password for the accounts using ServiceNow
            Change workspace.

            1.  Make sure you are in \"DevOps Data Model\" Scope /
                Application

## Plugins

DevOps Change Velocity is composed of three (3) applications, DevOps
Data Model, DevOps Integrations, and DevOps Insights. All three will
automatically install with DevOps Change Velocity.

<https://store.servicenow.com/sn_appstore_store.do#!/store/application/f1d62f041b3abc10d6f254a5624bcbf5/1.38.0?referer=%2Fstore%2Fsearch%3Flistingtype%3Dallinegrations%25253Bancillary_app%25253Bcertified_apps%25253Bcontent%25253Bindustry_solution%25253Boem%25253Butility%25253Btemplate%26q%3Ddevops%2520change%2520velocity&sl=sh> 

## Configuration

All platform dependencies are pre-installed when you install DevOps
Change Velocity. The system will activate the following related plugins
if they are not already active:

Dependencies

- ServiceNow IntegrationHub Runtime
    (com.glide.hub.integration.runtime)

- ServiceNow IntegrationHub Action Step - REST
    (com.glide.hub.action_step.rest)

- ServiceNow IntegrationHub Action Template - Data Stream
    (com.glide.hub.action_type.datastream)

- Legacy IntegrationHub Usage Dashboard
    (com.glide.hub.usage.dashboard)

### Install Plugin

Before you begin

Role required: admin.

Note: Before requesting or activating a plugin, check whether the plugin
has already been activated on your instance. For details on how to check
a plugin activation status, https://support.servicenow.com/kb?id=kb_article_view&sysparm_article=KB0678767 article in the Now Support knowledge base

- Navigate to All \> System Applications \> All Available
    Applications \> All.

- Find the plugin using the filter criteria and search bar. You can
    search for the plugin by its name or ID. If you cannot find a
    plugin, you might have to request it from ServiceNow personnel. 

- Select Install, and then in the Activate Plugin dialog box,
    select Activate. Note: When domain separation and delegated admin
    are enabled in an instance, the administrative user must be in
    the global domain. Otherwise, the following error
    appears: Application installation is unavailable because another
    operation is running Plugin Activation for \<plugin name\>. 

### Configure DevOps Change Velocity

Using workspace

- Navigate to Workspaces \> DevOps Change Workspace.

- From the Accounts and user's widget on the Home page, select Set up
    system accounts. You will be redirected to Administration \> Set up
    system accounts.

- In the DevOps integration user account section, select Set new
    password.![Setting up new integration user password through
    workspace](./media/image10.png)

- By default, the DevOps integration user is selected. You can also
    select another user from the list. ![Selecting a DevOps integration
    user through
    workspace](./media/image11.png)

- Note: The Username field will only list the users that have the
    sn_devops.integration role.

- Enter a password for the user and select Set password.![Selecting
    Set password for DevOps integration user account through
    workspace](./media/image12.png)

### Onboard Gitlab

We ca utilize any one of the following options to onboard GitLab.

- **Onboard GitLab to DevOps Change Velocity --- Workspace**

      - Connect your GitLab instance using the playbook in DevOps
        Change workspace to discover repositories and pipeline.

- **Onboard GitLab to DevOps Change Velocity --- Service Catalog**

      -   Create, connect, discover, and configure your GitLab instance
        using the ServiceNow Service Catalog.

- **Onboard GitLab to DevOps Change Velocity --- Classic**

      -   Connect your GitLab instance to discover, configure, and import
        repositories and pipelines.

- **Model a GitLab basic CI pipeline in DevOps.**

      -   Model a GitLab basic CI pipeline by mapping the pipeline to an
        app, and mapping DevOps pipeline steps to GitLab pipeline jobs.

- **Configuring the GitLab pipeline for DevOps**

      -   Webhooks are required in GitLab to send job and push
        notifications to DevOps. Change control can be configured
        in GitLab for a manual job.

- **Bulk commits in GitLab**

      -   Bulk commits are supported with GitLab.

### Onboard GitLab to DevOps Change Velocity --- Workspace

### Connect to a Tool 

Navigate to Workspaces \> DevOps Change Workspace

![A screenshot of a computer Description automatically
generated](./media/image13.png)

![](./media/image14.png)
Click on "Connect a tool"

Select a tool you want to integrate, In our case "GitLab" and provide a
unique name which is not available in the system already Ex:
GitLab\<\<YourprojectInitials\>\>

![](./media/image15.png)

Wait for it to open Playbook.

![](./media/image16.png)

Provide your GitLab instance URL and Access Token

See Section how to create token in GitLab on page number 12

Once you provide the credentials. You can click next.

Note: If your GitLab instance is on premises then you can utilize MID
Server as well.

![](./media/image17.png)

Above screenshot show you the access or permission you have at taken
while you have configured access token in GitLab

Click on Next

![](./media/image18.png)

You can specify Tool access here by providing Maintained By or simply
click Skip.

In the next step you will see or have option to configure Webhook
manually or automatically.

![](./media/image19.png)
Select the Project discovered and click on configure

Once webhook configured successfully. You will see below screen

![](./media/image20.png)

### Create an Application 

Click on "Create a new application" on previous screenshot

![](./media/image21.png)

Provide a unique Name ex: GitLabDemoApp

![](./media/image22.png)

Select the tool name you have created in previous step and the pipeline
which got discovered when you connected to the tool

![](./media/image23.png)

Click on "Associate Pipeline"

![A screenshot of a computer Description automatically
generated](./media/image24.png)

You can assign Services at each step and click on "Assign services"

![A screenshot of a computer Description automatically
generated](./media/image25.png)

![](./media/image26.png)

Click on skip and move to next screen

![](./media/image27.png)

Select the repository and associate it

![](./media/image28.png)

Now you have your tool connected and you have created a new Application
.

Now click on the Took on the left side of your workspace and select your
connected tool

![A screenshot of a computer Description automatically
generated](./media/image29.png)

Verify the configuration and connection alias

Click on pipeline tab and select your pipeline

![](./media/image30.png)

### Create DevOps Change

You can automatically create change requests from the CI/CD pipelines
and even automatically approve the change requests based on policies.

You can create change at any level you want of CI/CD Pipeline, to create
change select the step from the steps above for example.

Unit-test-job

![A screenshot of a computer Description automatically
generated](./media/image31.png)

Click on the change control and Change receipt

![A screenshot of a computer Description automatically
generated](./media/image32.png)

You can select configuration item, approval group, model and type of
change you want to create.

![](./media/image33.png)

You can select Template as well to create change.

Click on save.

Now go to your tool page by clicking on the tool icon on the left side.

![](./media/image34.png)

Click on Discover

![A screenshot of a computer Description automatically
generated](./media/image35.png)

Make sure you have clicked the checkbox "Owned by me" otherwise it will
start discovering all the repository from the Gitlab.

Click Submit.

![A screenshot of a computer Description automatically
generated](./media/image36.png)

You will have two import request created one for the code and another
for the orchestration

### View pipeline in ServiceNow

![](./media/image37.png)
# Resource

<https://docs.gitlab.com/>

<https://docs.servicenow.com/>

<https://www.servicenow.com/community/>

<https://www.youtube.com/@NOWsupport>

