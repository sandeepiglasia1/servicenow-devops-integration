#!/bin/bash

# Check if the branch is main
if [ "$CI_COMMIT_BRANCH" == "main" ]; then
  echo "Skipping pipeline check for main branch."
  exit 0
fi

# Set your GitLab credentials and project details
GITLAB_TOKEN="glpat-qTtGLdbcKwLtryFPynwv"
GITLAB_URL="https://gitlab.com"
PROJECT_ID=$CI_PROJECT_ID

# Set the GitLab API URL for open merge requests
URL="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests?state=opened"

# Get the list of open merge requests
OPEN_MERGE_REQUESTS=$(curl --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "${URL}")

# Get the current merge request IID from CI environment variables
CURRENT_MR_IID="${CI_MERGE_REQUEST_IID}"
echo "${CURRENT_MR_IID}"
# Print the IID of each open merge request
echo "${OPEN_MERGE_REQUESTS}" | jq -r '.[].iid'

# Check if there are any open merge requests excluding the current one
# Check if there are any open merge requests excluding the current one
if [ -n "${OPEN_MERGE_REQUESTS}" ]; then
  # Use grep to exclude the current MR_IID
  if echo "${OPEN_MERGE_REQUESTS}" | jq -r ".[].iid" | grep -q -v "${CURRENT_MR_IID}"; then
    echo "There are open merge requests excluding the current one. Please wait for them to be merged or closed before proceeding."
    exit 1
  else
    echo "No open merge requests excluding the current one. Proceeding with the merge."
  fi
else
  echo "No open merge requests. Proceeding with the merge."
fi


