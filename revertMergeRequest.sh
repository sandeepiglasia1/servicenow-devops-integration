#!/bin/bash

# Set your GitLab credentials and project details
GITLAB_TOKEN=$SK_CI_JOB_TOKEN
OLD_MERGE_REQUEST_IID=$1
GITLAB_URL="https://gitlab.com"

if [ "$CI_JOB_STATUS" == "failed" ]; then
# Get the source branch and target branch of the merge request
SOURCE_BRANCH=$(curl --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "${GITLAB_URL}/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${OLD_MERGE_REQUEST_IID}" | jq -r '.source_branch')
TARGET_BRANCH=$(curl --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "${GITLAB_URL}/api/v4/projects/${CI_PROJECT_ID}/merge_requests/${OLD_MERGE_REQUEST_IID}" | jq -r '.target_branch')

# Create a new branch for the revert
REVERT_BRANCH="revert-${TARGET_BRANCH}-to-${SOURCE_BRANCH}"

# Checkout the new branch
git checkout -b "${REVERT_BRANCH}" "origin/${TARGET_BRANCH}"


# Apply the changes from the source branch to the new branch
git cherry-pick "${SOURCE_BRANCH}"

# Push the new branch to GitLab
git push origin "${REVERT_BRANCH}"

# Create a new branch for the revert in GitLab
curl --request POST \
     --url "${GITLAB_URL}/api/v4/projects/${CI_PROJECT_ID}/repository/branches" \
     --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
     --data "branch=${REVERT_BRANCH}&ref=${TARGET_BRANCH}"

# Create a new merge request
title="Revert - Merge branch ${SOURCE_BRANCH} into ${TARGET_BRANCH}"
NEW_MERGE_REQUEST=$(curl --request POST \
     --url "${GITLAB_URL}/api/v4/projects/${CI_PROJECT_ID}/merge_requests" \
     --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" \
     --data "source_branch=${REVERT_BRANCH}&target_branch=${TARGET_BRANCH}&title=${title}&description=${title}")

# Extract the IID of the new merge request
NEW_MERGE_REQUEST_IID=$(echo "${NEW_MERGE_REQUEST}" | jq -r '.iid')
echo "New Merge Request IID:" ${NEW_MERGE_REQUEST_IID}
echo "Old Merge Request IID:" ${OLD_MERGE_REQUEST_IID}

# Update old merge request with the link to the new merge request

#LINK_TO_UPDATE="[Link to Reverted Merge Request](${GITLAB_URL}/${CI_PROJECT_PATH}/-/merge_requests/${NEW_MERGE_REQUEST_IID})"
LINK_TO_UPDATE=$(echo "CI/CD pipeline failed! Created new merge request to revert the changes:" "[Link to Reverted Merge Request](${GITLAB_URL}/${CI_PROJECT_PATH}/-/merge_requests/${NEW_MERGE_REQUEST_IID})")
curl --request POST --header "PRIVATE-TOKEN: $GITLAB_TOKEN" --data "body=$LINK_TO_UPDATE" "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/merge_requests/$OLD_MERGE_REQUEST_IID/notes"

fi
