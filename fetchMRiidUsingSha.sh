# Set the GitLab API URL
API_URL="https://gitlab.com/api/v4/projects/${CI_PROJECT_ID}/merge_requests"

# Set the commit SHA from the CI environment variable
COMMIT_SHA="$CI_COMMIT_SHA"

# Make the API request to fetch merge request ID
MR_IID=$(curl --header "PRIVATE-TOKEN: $SK_CI_JOB_TOKEN" "$API_URL" | jq --arg commit_sha "$COMMIT_SHA" '.[] | select(.merge_commit_sha == $commit_sha) | .iid')

# Print the merge request ID
echo "$MR_IID"
