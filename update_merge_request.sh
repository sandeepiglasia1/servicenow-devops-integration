#!/bin/bash

# GitLab API endpoint for updating a merge request
GITLAB_URL="https://gitlab.com"
PROJECT_ID=$1
MERGE_REQUEST_IID=$2
contentToUpdate=$3

GITLAB_API="${GITLAB_URL}/api/v4/projects/${PROJECT_ID}/merge_requests/${MERGE_REQUEST_IID}"

# GitLab personal access token
#ACCESS_TOKEN="glpat-e-484Nin5sDLWTgjpfFp"
ACCESS_TOKEN=$SK_CI_JOB_TOKEN

# Data to update in the merge request
NEW_DESCRIPTION="NEW_DESCRIPTION"

# cURL command to update the merge request
curl --request PUT \
  --url "${GITLAB_API}" \
  --header "PRIVATE-TOKEN: ${ACCESS_TOKEN}" \
  --data "{
    \"note\": \"${contentToUpdate}\",
    \"description\": \"${NEW_DESCRIPTION}\"
  }"
