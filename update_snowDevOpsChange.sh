#!/bin/bash

# ServiceNow API endpoint for updating a change request
#SERVICE_NOW_INSTANCE='dev183160.service-now.com'

# ServiceNow credentials
#USERNAME='admin'
#PASSWORD='l9L5f@Mg@cFL'

SERVICE_NOW_INSTANCE=$SK_SERVICENOW_INSTANCE_FULL
USERNAME=$SK_SERVICENOW_USERNAME
PASSWORD=$SK_SERVICENOW_PASSWORD

CHANGE_REQUEST_ID=$1
updatedShortDescription=$2


# API EndPoint 
SERVICE_NOW_API="https://${SERVICE_NOW_INSTANCE}/api/now/table/change_request/${CHANGE_REQUEST_ID}"


# cURL command to update the change request
curl --request PUT \
  --url "${SERVICE_NOW_API}" \
  --header "Accept: application/json" \
  --header "Content-Type: application/json" \
  --user "${USERNAME}:${PASSWORD}" \
  --data "{
    \"short_description\": \"${updatedShortDescription}\",
    \"description\": \"${updatedShortDescription}\"
  }"
